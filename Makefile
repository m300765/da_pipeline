
# Makefile to install/deploy da-pipeline
#
PATH_PREFIX?=$(PWD)
DEVELOP?=
ENV_PATH=$(PATH_PREFIX)/env
PYTHON?=3.8
CHANNEL?=conda-forge

PIP_FLAG:=--develop
ifeq ($(strip $(DEVELOP)),)
	PIP_FLAG:=
endif

deploy:
	echo Installing into $(ENV_PATH)
	rm -fr $(ENV_PATH)
	mkdir -p $(ENV_PATH)
	./setup_conda --install-prefix $(ENV_PATH) --channel $(CHANNEL) --python $(PYTHON) --develop

install:
	python3 -m pip install --user .

develop:
	python3 -m pip install --user -e .
