"""Collection of methods to regrid and read 2D datasets."""

import abc
from itertools import chain
import logging
from pathlib import Path
import os
from tempfile import NamedTemporaryFile, TemporaryDirectory

import dask
import pandas as pd
import numpy as np
import xarray as xr

from ..utils import (_get_enc,
                     run_cmd,
                     SCRATCH_DIR,
                     Progress,
                     icon2datetime,
                     get_filenames,
                     wait_for_workers)


__all__ = ('Regridder', 'SingleLevelWriter' )

logger = logging.getLogger(name='SingleLevelWriter')

class Regridder:
    """Base class that provides a common interface for 3D and 2D data processing."""

    min_threads = 24
    threads_per_worker = 12
    _grid_file = Path('/pool/data/ICON/grids/public/mpim/0015/icon_grid_0015_R02B09_G.nc')

    @abc.abstractmethod
    def __init__(self, inp_path, output, exp_name, glob_pattern, *,
                 target_res=[0.5, 0.5],
                 center_lon=0,
                 start=None,
                 end=None,
                 grid_file=None,
                 avg_period='1d',
                 cdo='cdo',
                 gridtype='2d',
                 **kwargs):
        self.gridtype = gridtype
        self.avg_period = avg_period
        self._client = None
        self._partition = None
        self.dy, self.dx = target_res
        self.center_lon = center_lon
        self.lookup = None
        self.exp_name = exp_name
        self.glob_pattern = glob_pattern
        self.grid_file = grid_file or self._grid_file
        self.cdo = cdo
        self.output = Path(output).expanduser().absolute()
        self.inp_path = Path(inp_path).expanduser().absolute()
        self.output.mkdir(exist_ok=True, parents=True)
        self.files = sorted([f for f in get_filenames(self.inp_path,
                                                      self.exp_name,
                                                      self.gridtype,
                                                      start,
                                                      end) if (self.inp_path / Path(f).name).is_file()])
        self.s_dates = pd.DatetimeIndex(sorted(set([Path(f).with_suffix('').name.split('_')[-1] for f in self.files])))
        self.start = self.convert_ts(start or self.s_dates[0])
        self.end = self.convert_ts(end or self.s_dates[-1])
        self.dates = pd.date_range(
                        pd.Timestamp(self.start),
                        pd.Timestamp(self.end),
                        freq='1D')
        self._td = TemporaryDirectory(dir=SCRATCH_DIR, prefix=f'{self.exp_name}Corrector{self.gridtype}')
        self.tmp = Path(self._td.name)
        self.tmp.mkdir(exist_ok=True, parents=True)
        self._weight_file, self._griddes_file = None, None


    @staticmethod
    @dask.delayed
    def gen_dis(dataset, grid_file, griddes, cdo, scratch_dir=None, global_attrs={}, **kwargs):
        """Create a distance weights using cdo.

        Parameters:
        ===========
            dataset, xarray.DataArray, xarray.Dataset : input data on icon-grid
            grid_file, str, pathlib.Path : file containing the icon grid
            griddes, str, pathlib.Path : file containing target grid description
            scratch_dir, str, pthlib.Path : directory for temporary directories (default: None)
            global_attrs, dict : Additional attributes applied to the dataset

        Returns:
        ========
            pathlib.Path : filename of the (netcdf) weightfile
        """

        if isinstance(dataset, xr.DataArray):
            # If a dataArray is given create a dataset
            dataset = xr.Dataset(data_vars={dataset.name: dataset})
            dataset.attrs = global_attrs
        weightfile = NamedTemporaryFile(dir=scratch_dir, prefix='weight_file', suffix='.nc').name
        scratch_dir = scratch_dir or SCRATCH_DIR
        with NamedTemporaryFile(dir=scratch_dir, prefix='input_', suffix='.nc') as tf:
            if not isinstance(dataset, (str, Path)):
                enc = _get_enc(dataset).compute()
                write_future = dataset.to_netcdf(tf.name, mode='w', encoding=enc, compute=True, engine='h5netcdf')
                out_f = tf.name
            else:
                out_f = dataset
            cmd = (cdo, '-O', f'gendis,{griddes}', f'-setgrid,{grid_file}',
                    out_f, weightfile)
            run_cmd(cmd).compute()
            return weightfile

    @staticmethod
    def get_grid_from_dataset(dataset, cdo, scratch_dir=None):
        """Get cdo confrom grid-description of a dataset."""

        scratch_dir = scratch_dir or SCRATCH_DIR
        if isinstance(dataset, xr.DataArray):
            dataset = xr.Dataset(data_vars={dataset.name: dataset})
        sel_dims = [dim for dim in dataset.dims.keys() if dim not in ('lon', 'lat')]
        if sel_dims:
            dataset = dataset.isel({dim:0 for dim in sel_dims}).drop(sel_dims)
        with NamedTemporaryFile(dir=SCRATCH_DIR, prefix='Griddes_', suffix='.nc') as tf:
            dataset.to_netcdf(tf.name)
            cmd = (cdo, 'griddes', tf.name)
            del dataset
            return run_cmd(cmd).compute()

    @property
    def workers(self):
        """Return the number of workers from a distributed client."""

        return sum([w['nthreads'] // self.threads_per_worker
            for w in self._worker_info.values()])

    @property
    def njobs(self):
        """Calculate the number of days to work in parallel."""

        sizes_per_day = []
        for day, glob_dict in self.partition.items():
            sizes_per_day.append(sum([os.path.getsize(f) for f in
                chain.from_iterable(glob_dict.values())]))
        if len(sizes_per_day) == 0:
            raise FileNotFoundError('No Files Found, check glob pattern')
        avg_size_per_day = sum(sizes_per_day) / len(sizes_per_day)
        num_days_per_worker = self.memory_limit / avg_size_per_day
        num = int(min(self.workers, num_days_per_worker*len(self._worker_info)))
        return num

    @property
    def _worker_info(self):
        if self._client is None:
            self._client = wait_for_workers(self.min_threads, self.threads_per_worker)
        return self._client._scheduler_identity.get('workers', {})

    @property
    def memory_limit(self):
        """Get the memory a worker can take."""

        workers = self._worker_info
        return sum([w['memory_limit'] for w in workers.values()]) / len(workers)

    def get_coords(self, dset):
        """Get coordinates for dataset on lat-lon grid.

        Parameters:
        ===========

            dset, xr.DataArray : Inpt dataset on icon grid

        Returns:
        ========
            dict :  coords Coordinates of the dataset on the lon-lat grid
        """

        xsize, ysize = self.xysize
        xfirst = self.center_lon + self.dx / 2
        yfirst = -90 + self.dy / 2
        lonlat_attrs = {'lon': {'long_name': 'longitued', 'units': 'degrees_east'},
                        'lat': {'long_name': 'latitude', 'units': 'degrees_north'}}
        coords = dict(
            lon=xr.IndexVariable('lon',
                                 np.arange(xfirst, (xfirst + xsize * self.dx), self.dx),
                                 attrs=lonlat_attrs['lon']),
            lat=xr.IndexVariable('lat',
                                 np.arange(yfirst, (yfirst + ysize * self.dy), self.dy),
                                 attrs=lonlat_attrs['lat'])
        )
        if 'cells' in dset.dims or 'ncells' in dset.dims:
            return {**coords, **{k:v for k, v in dset.coords.items() if k not in ('cells', 'ncells')}}
        return {k:v for k, v in dset.coords.items()}

    @property
    def xysize(self):
        """Return xysize for given resolution."""

        return int(round(360 / self.dx, 0)), int(round(180 / self.dy, 0))

    @property
    def griddes(self):
        """Create grid description file.

        Returns:
        ========

        pathlib.Path : Path to grid descriiption file
        """

        xsize, ysize = self.xysize
        xfirst = self.center_lon + self.dx / 2
        yfirst = -90 + self.dy / 2
        griddes = f"""#
# gridID 1
#
gridtype  = lonlat
gridsize  = {xsize*ysize}
datatype  = float
xsize     = {xsize}
ysize     = {ysize}
xname     = lon
xlongname = "longitude"
xunits    = "degrees_east"
yname     = lat
ylongname = "latitude"
yunits    = "degrees_north"
xfirst    = {xfirst}
xinc      = {self.dx}
yfirst    = {yfirst}
yinc      = {self.dy}
    """
        return griddes

    @staticmethod
    @dask.delayed
    def remap(dataArray, grid_file, griddes, weightfile, tmpdir, chunks, cdo, attrs={}, **kwargs):
        """Perform a weighted remapping.

        Parameters:
        ===========

        dataArray : xarray.dataArray
            The dataset the will be regriddes
        grid_file : pathlib.Path, str
            Path to the grid file
        griddes : pathlib.Path, str
            Path to the grid describtion file
        tmpdir : pathlib.Path, str
            Directory for where temporary fils are stored

        Returns:
        ========
        numpy.array : df Remapped dataset
        """

        if not isinstance(dataArray, xr.DataArray):
            raise TypeError('dataArray must be of %s' %xr.DataArray)
            # If a dataArry is given create a dataset
        try:
            del attrs['history']
        except KeyError:
            pass
        dataset = xr.Dataset(data_vars={dataArray.name: dataArray}, attrs=attrs)
        enc = _get_enc(dataset).compute()
        with NamedTemporaryFile(dir=tmpdir, prefix='cdo_', suffix='.nc') as infile:
            with NamedTemporaryFile(dir=tmpdir, prefix='cdo_', suffix='.nc') as outfile:
                dataset.to_netcdf(infile.name, encoding=enc, mode='w', engine='h5netcdf')
                # Create the command to run
                cmd = (cdo, '-O', f'remap,{griddes},{weightfile}',
                       f'-setgrid,{grid_file}',
                       str(infile.name), str(outfile.name))
                # Get the return value of the command
                run_cmd(cmd).compute()
                df = xr.open_dataset(outfile.name, chunks=chunks).load()
                return df[dataArray.name].values

    @staticmethod
    @dask.delayed
    def remap_from_file(inp_files, out_file, weight_file, griddes, grid_file, cdo, **kwargs):
        """Call a cdo ramp on a given input file."""

        if Path(out_file).is_file():
            return out_file
        cmd = (f'{cdo} -O mergetime',
              f'-apply,-remap,{griddes},{weight_file}',
              f'[ {" ".join(inp_files)} ] {out_file}'
               )
        cmd = ' '.join(cmd)
        return run_cmd(cmd, **kwargs).compute()

    @staticmethod
    def get_enc(dataset, missing_val=-99.99e36):
        """Get encoding to save datasets."""

        enc = {}
        for varn in dataset.data_vars:
            enc[varn] = {'_FillValue': missing_val}
            dataset[varn].attrs = {**dataset[varn].attrs,
                                   **{'missing_value': missing_val}}
        return enc, dataset

    def get_globpattern(self, filename, exp_name, glob_pattern):
        """Get the glob pattern of a given filename."""

        filename = Path(filename).name
        date = filename.split('_')[-1]
        plg = filename.replace(f'{exp_name}', '').replace(date, '').strip('_')
        if plg not in glob_pattern:
            split_pat = plg.split('_')
            for ii, s in enumerate(split_pat):
                if s == self.gridtype:
                    plg = '_'.join(split_pat[ii:]).strip('_')
        if  plg in glob_pattern:
            return plg

    def get_partition(self):
        """Partition a list of input files by days and glob pattern."""

        dates_data = {}
        for f in sorted(set(self.files)):
            date = pd.Timestamp(Path(f).with_suffix('').name.split('_')[-1]).strftime('%Y%m%d')
            glp = self.get_globpattern(str(f), self.exp_name, self.glob_pattern)
            if glp is None:
                continue
            if date in dates_data:
                if glp in dates_data[date]:
                    dates_data[date][glp].append(f)
                else:
                    dates_data[date][glp] = [f]
            else:
                dates_data[date] = {glp: [f]}
        return dates_data

    def _traverse_futures(self, futures):
        gather_results = dask.delayed(
                    lambda new_future, old_future: dask.compute(list(new_future)+list(old_future))[::-1][0]
                    )
        results = (gather_results(futures[0], []), )
        if len(futures) == 0:
            return results[-1]
        for future in futures[1:]:
            if len(futures):
                results += (gather_results(results[-1], future), )
        return (results[-1], )

    def distribute(self):
        """Setup a dask task stream and distribute it on the cluster."""

        prep = self.prepare()
        return self._traverse_futures(prep), len(prep)

    @staticmethod
    def convert_ts(ts):
        """Convert a date string to a pandas Timestamp."""

        try:
            return pd.Timestamp(ts).tz_convert(None)
        except TypeError:
            return pd.Timestamp(ts)

    @property
    def weight_file(self):
        """Create a weight files."""

        if self._weight_file is None:
            weight_future = self.gen_dis(self.files[0],
                                      self.grid_file,
                                      self.griddes_file,
                                      self.cdo,
                                      scratch_dir=self.tmp).persist()
            Progress(weight_future, label='gendis', notebook=False)
            self._weight_file = weight_future.compute()
        return self._weight_file

    def get_griddes(self):
        """Create grid description file.

        Returns:
        ========

        pathlib.Path : Path to grid descriiption file
        """
        return griddes_file()

    @property
    def griddes_file(self):
        """Create the grid description file."""

        if self._griddes_file is None:
            self._griddes_file = Path(self.tmp) / 'girddes.txt'
            with self._griddes_file.open('w') as f:
                f.write(self.griddes)
        return self._griddes_file

    def get_shape(self, dset):
        """Get the shape of a dataset on lon-lat grid.

        Parameters:
        ===========

            dset, xarray.DataArray : input dataArray on icon-grid

        Returns:
        ========
            dict : dimensions of the same dataArray on lat-lon grid
        """

        xsize, ysize = self.xysize
        df = xr.Dataset(data_vars={dset.name:dset})
        dims = {d:df.dims[d] for d in dset.dims}
        geo_var = False
        for cells_dim in ('ncells', 'cells'):
            try:
                del dims[cells_dim]
                geo_var = True
            except KeyError:
                pass
        if geo_var:
            dims =  {**dims, **{'lat':ysize, 'lon':xsize},}
        return dims

    def get_chunk(self, dataArray):
        """Get chunksize of dataset on lon-lat grid.
        Note: The chunk length of the time dimension will be set to 1.

        Parameters:
        ===========
            dataArray, xarray.DataArray : input dataArray on icon-grid

        Returns:
        ========
            dict : chunk dimensions of the same dataArray on icon-grid
        """
        _chunks, chunks = {}, {}
        height_var = None
        if 'time' in dataArray.dims:
            _chunks['time'] = 1
        xsize, ysize = self.xysize
        for dim in dataArray.dims:
            if dim in ('cells', 'ncells'):
                continue
            try:
                chunks[dim] = _chunks[dim]
            except KeyError:
                chunks[dim] = -1
        if 'ncells' not in dataArray.dims and 'cells' not in dataArray.dims:
            return chunks
        return {**chunks, **{'lat':min(ysize, 180), 'lon': min(xsize, 360)}, }

    def apply(self):
        """Run the 3D data reduction."""

        self.futures = None
        tasks, n_batches = self.distribute()
        futures = dask.persist(*tasks)
        Progress(futures,
                label=f'Creating new files ({n_batches} batches)' ,
                notebook=False)
        self.out_files = tuple(chain.from_iterable(dask.compute(futures)[0]))

    @property
    def partition(self):
        if self._partition is None:
            self._partition = self.get_partition()
        return self._partition



    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self._cleanup()

    def _cleanup(self):
        """Delete griddes and weightfile."""

        for file in (self.griddes_file, self.weight_file):
            try:
                Path(file).unlink()
            except (FileNotFoundError, ValueError, TypeError):
                pass
        self._td.cleanup()

class SingleLevelWriter(Regridder):
    """Class that processes 2D data."""
    lookup_acc = {
                'accsob_s': 'nswrf_s',
                'accthb_s': 'nlwrf_s',
                'accsob_t': 'nswrf_t',
                'accthb_t': 'nlwrf_t',
                'accsod_t': 'rsdt',
                'accthu_s': 'rlds',
                'acclhfl_s': 'hfls',
                'accshfl_s': 'hfss',
                'tot_prec': 'pr',
                'accsodifd_s': 'sodifd_s',
                'accsodird_s':'sodird_s',
            }
    lookup_avg = {
                'u_10m': 'uas',
                'v_10m': 'vas',
                't_2m': 'tas',
                'tqv_dia':'prw',
                'tqc_dia':'cllvi',
                'tqi_dia':'clivi',
                'tqg':'qgvi',
                'tqr':'qrvi',
                'tqs':'qsvi',
                'clct': 'clt',
                't_g':'ts',
                'qv_2m': 'qas',
                'qv_s': 'qs',
                'pres_sfc': 'pres',

            }

    def __init__(self, inp_path, output, exp_name, glob_pattern, *,
                 target_res=[0.5, 0.5],
                 center_lon=0,
                 start=None,
                 end=None,
                 grid_file=None,
                 avg_period='1d',
                 cdo='cdo',
                 **kwargs):
        """Setup a Pipeline for processing 2D data.

        Parameters:
        ==========
            inp_path: str, pathlib.Path
                Input path where the experiment data is stored
            output: str, pathlib.Path
                Output path where data gets stored
            exp_name: str
                Experiment ID
            glob_pattern: tuple, list
                collection of glob_patterns for input files that should
                be processed.
            target_res: tuple, list, default: (0.5, 0.5)
                y,x resolution in degrees of the output lat-lon gird
                (y, x)
            center_lon: int, default: 0
                longitude of the center
            start: str, pandas.Timestamp, default: None
                first time stamp that is processed, if None is given (default)
                the first time stamp that is found is taken
            end: str, pandas.Timestamp, default: None
                last time stamp that is processed, if None is given (default)
                the last time stamp that is found is taken
                vertical interpolation is applied.
            avg_period: str, default: 1d
                window that is applied for averaging
        """
        self.runtype = kwargs.get('run_type', 'dpp')
        self.varnames = kwargs.get('data_vars_2d', '') or None
        super().__init__(inp_path, output, exp_name, glob_pattern,
                         target_res=target_res,
                         center_lon=center_lon,
                         start=start,
                         end=end,
                         grid_file=grid_file,
                         avg_period=avg_period,
                         cdo=cdo,
                         gridtype='2d')

    def prepare(self):
        """Prepare the dask task stream to apply the pipeline task."""

        queue = []
        wrk_dir1 = self.tmp / 'wrkdir'
        wrk_dir2 = self.tmp / 'outdir'
        wrk_dir1.mkdir(exist_ok=True, parents=True)
        wrk_dir2.mkdir(exist_ok=True, parents=True)
        out_dir = self.output / self.exp_name
        out_dir.mkdir(exist_ok=True, parents=True)
        for date, glob_data in self.partition.items():
            day_queue = []
            for glob_pattern, files in glob_data.items():
                remap_files = sorted(files)
                if len(remap_files) == 0:
                    continue
                if '_atm_' in remap_files[0].lower():
                    glob = 'atm'
                else:
                    glob = 'oce'
                if '3d' in remap_files[0].lower():
                    glob += '_3d'
                else:
                    glob += '_2d'
                datestr = Path(remap_files[0]).with_suffix('').name.split('_')[-1]
                out_f = out_dir / f'{self.exp_name}_{glob}_{datestr}.nc'
                remap_f = wrk_dir1 / f'{self.exp_name}_{glob_pattern}_{datestr}.nc'
                timmean_f = wrk_dir2 / remap_f.name
                remap_job = self.remap_from_file(remap_files,
                                                 remap_f,
                                                 self.weight_file,
                                                 self.griddes_file,
                                                 self.grid_file,
                                                 self.cdo,
                                                 returnVal=remap_f)
                timmean_job = self.timmean(remap_job,
                                           timmean_f,
                                           self.avg_period,
                                           runtype=self.runtype)
                day_queue.append(timmean_job)
            if len(day_queue) and not out_f.is_file():
                merge_job = self.merge(day_queue,
                                       out_dir,
                                       self.exp_name,
                                       out_f,
                                       varnames=self.varnames)
                queue.append(merge_job)
        return np.array_split(queue, max(len(queue) // self.njobs, 1))

    @staticmethod
    def _preproc(dset):
        """Pre-process method before combining the data."""

        global_attrs = dset.attrs
        attrs = {varn: dset[varn].attrs for varn in dset.data_vars}
        for lev in ('height_2', 'height', 'depth', 'depth_2'):
            try:
                if dset.dims[lev] == 1:
                    dset = dset.isel({lev: 0}).drop(lev)
            except (KeyError, IndexError):
                pass
        for coord in ('clon', 'clat'):
            try:
                dset = dset.drop(coord)
            except (KeyError, ValueError):
                pass
        for varn in dset.data_vars:
            dset[varn].attrs = attrs[varn]
        dset.attrs = global_attrs
        try:
            return dset.drop_vars('time_bnds')
        except ValueError:
            return dset


    @staticmethod
    @dask.delayed
    def merge(filenames, outdir, exp_name, out_f, varnames=None, engine='h5netcdf'):
        """Merge files from different glob patterns togehter."""

        dset = xr.open_mfdataset(sorted(filenames),
                                 parallel=False,
                                 combine='by_coords')
        dset = dset.assign_coords({'time': icon2datetime(dset.time)})
        varnames = varnames or list(dset.data_vars)
        write_future = dset[varnames].to_netcdf(out_f, engine=engine, mode='w', compute=False).compute()
        return out_f

    @staticmethod
    @dask.delayed
    def timmean(inp_file, out_file, avg_period, **kwargs):
        """Apply time average over a given input period."""

        if Path(out_file).is_file():
            return out_file
        dset_in = xr.open_mfdataset([str(inp_file)],
                                    combine='by_coords',
                                    parallel=False,
                                    preprocess=SingleLevelWriter._preproc
                                    ).persist()
        dset_out = dset_in.assign_coords({'time': icon2datetime(dset_in.time)})
        attrs = dset_in.attrs
        run_type = kwargs.get('runtype', 'dpp')
        dvars = list(dset_out.data_vars)
        v_attrs = {v:dset_out[v].attrs for v in dvars}
        if run_type.startswith('nwp'):
            lookup = {}
            for v in dvars:
                if v in SingleLevelWriter.lookup_acc:
                    lookup[v] = SingleLevelWriter.lookup_acc[v]
                    if lookup[v] == 'pr':
                        dset_out[v].attrs['units'] = 'kg/m^2/s'
                    else:
                        dset_out[v].attrs['units'] = 'W/m^2'
                elif v in SingleLevelWriter.lookup_avg:
                    lookup[v] = SingleLevelWriter.lookup_avg[v]
                else:
                    lookup[v] = v
                if v in list(SingleLevelWriter.lookup_acc.keys()):
                    dt = (pd.Timestamp(dset_out.time[1].values) - pd.Timestamp(dset_out.time[0].values)).total_seconds()
                    dset_out[v] = dset_out[v].diff(dim='time') / dt
                elif v == 'clct':
                    dset_out[v].data /= 100.
                    dset_out[v].attrs['units'] = 'frac.'
            dset_out = dset_out.rename(lookup).isel({'time':slice(0, len(dset_out.time)-1)})
            dvars = list(dset_out.data_vars)
            if 'nlwrf_t' in dvars:
                coords = dset_out['nlwrf_t'].coords
                dims = dset_out['nlwrf_t'].dims
                dset_out['rlut'] = xr.DataArray(-dset_out['nlwrf_t'].data,
                                                name='rlut',
                                                coords=coords,
                                                dims=dims,
                                                attrs={'long_name':'TOA Outgoing longwave radiation',
                                                       'units': 'W/m^2'})
            if 'rsdt' in dvars and 'nswrft_t' in dvars:
                coords = dset_out['nswrf_t'].coords
                dims = dset_out['nswrf_t'].dims
                dset_out['rsut'] = xr.DataArray(dset_out['rsdt'].data - dset_out['nswrf_t'].data,
                                                name='rsut',
                                                dims=dims,
                                                coords=coords,
                                                attrs={'long_name': 'TOA Outgoing shortwave radiation',
                                                       'units': 'W/m^2'})
            v_attrs = {v:dset_out[v].attrs for v in dset_out.data_vars}
        if avg_period:
            dset_out = dset_out.resample(time=avg_period).mean()
        for data_var, attrs in v_attrs.items():
            dset_out[data_var].attrs = attrs
        dset_out.attrs = dset_in.attrs
        write_future = dset_out.to_netcdf(out_file,
                                          engine='h5netcdf',
                                          mode='w',
                                          compute=False).compute()
        return str(out_file)
