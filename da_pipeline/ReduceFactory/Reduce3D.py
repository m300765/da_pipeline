"""Collection of methods that process 3D data."""

from copy import deepcopy
import logging
from pathlib import Path
import os
from tempfile import NamedTemporaryFile, TemporaryDirectory

import dask
import pandas as pd
import numpy as np
import stratify
import xarray as xr

from ..utils import icon2datetime, Progress

from .Reduce2D import Regridder

__all__ = ('MultiLevelWriter', )

logger = logging.getLogger(name='MultiLevelWriter')



class MultiLevelWriter(Regridder):
    """Class that processes 3D data."""

    _lookup_a = {'P': 'pfull',
                 'QV': 'hus',
                 'T': 'ta',
                 'QC_DIA': 'clw',
                 'QI_DIA': 'cli',
                 'U': 'ua',
                 'V': 'va',
                 'W': 'w'}
    _lookup_b = {'pres': 'pfull',
                 'qv': 'hus',
                 'temp':'ta',
                 'tot_qc_dia': 'clw',
                 'tot_qi_dia': 'cli',
                 'u': 'ua',
                 'v': 'va',
                 'w': 'w'}

    min_height = 25
    max_height = 35000

    @staticmethod
    @dask.delayed
    def timmean(inp_file, out_file, avg_period, **kwargs):
        """Apply time average over a given input period."""

        if Path(out_file).is_file():
            return out_file
        dset_in = xr.open_mfdataset([str(inp_file)], combine='by_coords').persist()
        attrs = dset_in.attrs
        dset_out = dset_in.assign_coords({'time': icon2datetime(dset_in.time)})
        if avg_period:
            dset_out = dset_out.resample(time=avg_period).mean()
        for data_var in dset_in.data_vars:
            dset_out[data_var].attrs = dset_in[data_var].attrs
        dset_out.attrs = dset_in.attrs
        write_future = dset_out.to_netcdf(out_file,
                                          engine='h5netcdf',
                                          mode='w',
                                          compute=False).compute()
        return str(out_file)

    def __init__(self, inp_path, output, exp_name, glob_pattern, *,
                 target_res=[0.5, 0.5],
                 center_lon=0,
                 start=None,
                 end=None,
                 vgrid_file=None,
                 grid_file=None,
                 min_height=0,
                 max_height=35000,
                 avg_period='1d',
                 cdo='cdo',
                 **kwargs):
        """Setup a Pipeline for processing 3D data.

        Parameters:
        ==========
            inp_path: str, pathlib.Path
                Input path where the experiment data is stored
            output: str, pathlib.Path
                Output path where data gets stored
            exp_name: str
                Experiment ID
            glob_pattern: tuple, list
                collection of glob_patterns for input files that should
                be processed.
            target_res: tuple, list, default: (0.5, 0.5)
                y,x resolution in degrees of the output lat-lon gird
                (y, x)
            center_lon: int, default: 0
                longitude of the center
            start: str, pandas.Timestamp, default: None
                first time stamp that is processed, if None is given (default)
                the first time stamp that is found is taken
            end: str, pandas.Timestamp, default: None
                last time stamp that is processed, if None is given (default)
                the last time stamp that is found is taken
            vgrid_file: str, pathlib.Path, default: None
                Path to the file containing information on the z - level
                for vertical interpolation. If None given (default), no
                vertical interpolation is applied.
            min_height: float, default: 25
                height [m] of the lowest iso-z level
            max_height: float, default: 35000
                height [m] of the upper most iso-z level
            avg_period: str, default: 1d
                window that is applied for averaging
        """
        self.vgrid_file = vgrid_file or None
        self.min_height = min_height
        self.max_height = max_height
        super().__init__(inp_path, output, exp_name, glob_pattern,
                         target_res=target_res,
                         center_lon=center_lon,
                         start=start,
                         end=end,
                         grid_file=grid_file,
                         avg_period=avg_period,
                         cdo=cdo,
                         gridtype='3d')
        if self.exp_name == 'nwp':
            self.lookup = self._lookup_a
        if self.exp_name == 'nwp0005':
            self.lookup = self._lookup_b
        self.varnames = list(self._lookup_a.values())
        if self.lookup is None:
            self.varnames = list(self._lookup_a.values())[:-1] + ['wap']
        varnames = kwargs.get('data_vars_3d', '') or None
        if varnames:
            if isinstance(varnames, str):
                varnames = [varnames]
            self.varnames = list(varnames)
        self._Z, self._z_data = None, None

    @property
    def z_data(self):
        """Get the information of the Z data."""

        if self.vgrid_file is None:
            return None
        if self._z_data is None:
            vgrid_file = self.tmp / 'vgrid_file.nc'
            vgrid_lonlat = self.remap_from_file([str(self.vgrid_file)],
                    vgrid_file,
                    self.weight_file,
                    self.griddes_file,
                    self.grid_file,
                    self.cdo
                    ).persist()
            Progress(vgrid_lonlat, label='Remapping vgrid', notebook=False)
            vgrid_lonlat = vgrid_lonlat.compute()
            z_data = xr.open_mfdataset([vgrid_file], combine='by_coords')
            z_data['zg'].data = (z_data['zg'].data - z_data['zg'].isel({'height_2': -1}).data) + self.min_height
            z_data['zghalf'].data = (z_data['zghalf'].data - z_data['zghalf'].isel({'height': -1}).data) + self.min_height
            self._z_data = z_data[['zg', 'zghalf']]
        return self._z_data

    @property
    def Z(self):
        """Calculate the target iso-Z level."""

        if self.vgrid_file is None:
            return None
        if self._Z is None:
            height = xr.apply_ufunc(np.fabs, deepcopy(self.z_data['zg'].isel({'height_2': -1}).load()))
            target = height.where(height==height.min() , drop=True).drop('height_2').coords
            height = self.z_data['zg'].sel(target).isel({'lon':0, 'lat':0})
            self._Z = height.where(height <= self.max_height, drop=True).round(0).data
        return self._Z

    @classmethod
    @dask.delayed
    def merge(cls, filenames, outdir, exp_name, out_f, zhash, varnames=None, lookup={}):
        """Merge files from different glob patterns togehter."""

        dset = xr.open_mfdataset(sorted(filenames),
                                 parallel=True,
                                 combine='by_coords').rename(lookup)
        dset = dset.assign_coords({'time': icon2datetime(dset.time)})
        varnames = varnames or list(dset.data_vars)
        dset = cls.vint(dset[varnames], zhash)
        write_future = dset.to_netcdf(out_f, engine='h5netcdf', mode='w', compute=False).compute()
        return out_f

    @staticmethod
    def vint(dset, zhash):
        """Do the vertical interpolation to iso-z level."""

        if zhash['Z'] is None or zhash['z_data'] is None:
            return dset
        new_data = {}
        for varn in dset.data_vars:
            if 'lat' not in dset[varn].dims or 'lon' not in dset[varn].dims:
                continue
            # Do the interpolation on the cluster with dask.delayed
            z_var, z_dim = 'zg', 'height_2'
            if varn.lower() in ('w', 'v', 'u'):
                z_var, z_dim = 'zghalf', 'height'
            z_dimt = dset[varn].dims[1]
            z_coords = zhash['z_data'][z_var].coords
            sel = (len(z_coords[z_dim]) - len(dset.coords[z_dimt]), len(z_coords[z_dim]))
            h_data = xr.concat(dset.time.shape[0]*[zhash['z_data'][z_var].isel({z_dim: slice(*sel)})], dim='time')
            shape = dset.coords['time'].shape + zhash['Z'].shape + dset.coords['lat'].shape + dset.coords['lon'].shape
            tmp = dask.delayed(stratify.interpolate)(zhash['Z'], h_data.data, dset[varn].data, axis=1)
            # Create an xarray data array from the delayed object
            da = dask.array.from_delayed(tmp, shape=shape, dtype=np.float32)
            new_data[varn] = xr.DataArray(da,
                                          name=varn,
                                          dims=('time', 'height', 'lat', 'lon'),
                                          coords={'time': dset.time,
                                          'height': xr.IndexVariable('height', zhash['Z'], attrs={'long_name': 'Height', 'units': 'm'}),
                                          'lon': dset.coords['lon'],
                                          'lat': dset.coords['lat']},
                                           attrs=dset[varn].attrs).chunk({'time':1})
        return xr.Dataset(data_vars=new_data, attrs=dset.attrs, coords=new_data[varn].coords)

    def prepare(self):
        """Prepare the dask task stream to apply the pipeline task."""

        queue = []
        zhash = {'z_data':self.z_data, 'Z':self.Z}
        wrk_dir1 = self.tmp / 'wrkdir'
        wrk_dir2 = self.tmp / 'outdir'
        wrk_dir1.mkdir(exist_ok=True, parents=True)
        wrk_dir2.mkdir(exist_ok=True, parents=True)
        out_dir = self.output / self.exp_name
        out_dir.mkdir(exist_ok=True, parents=True)
        for date, glob_data in self.partition.items():
            day_queue = []
            for glob_pattern, files in glob_data.items():
                remap_files = sorted(files)
                if len(remap_files) == 0:
                    continue
                if '_atm_' in remap_files[0].lower():
                    glob = 'atm'
                else:
                    glob = 'oce'
                if '3d' in remap_files[0].lower():
                    glob += '_3d'
                else:
                    glob += '_2d'
                datestr = Path(remap_files[0]).with_suffix('').name.split('_')[-1]
                out_f = out_dir / f'{self.exp_name}_{glob}_{datestr}.nc'
                remap_f = wrk_dir1 / f'{self.exp_name}_{glob_pattern}_{datestr}.nc'
                timmean_f = wrk_dir2 / remap_f.name
                remap_job = self.remap_from_file(remap_files,
                                                 remap_f,
                                                 self.weight_file,
                                                 self.griddes_file,
                                                 self.grid_file,
                                                 self.cdo,
                                                 returnVal=remap_f)
                timmean_job = self.timmean(remap_job, timmean_f, self.avg_period)
                day_queue.append(timmean_job)
            if len(day_queue) and not out_f.is_file():
                merge_job = self.merge(day_queue,
                                       out_dir,
                                       self.exp_name,
                                       out_f,
                                       zhash,
                                       varnames=self.varnames,
                                       lookup=self.lookup or {})
                queue.append(merge_job)
        return np.array_split(queue, max(len(queue) // self.njobs, 1))

    @staticmethod
    @dask.delayed
    def timmean(inp_file, out_file, avg_period, **kwargs):
        """Apply time average over a given input period."""

        if Path(out_file).is_file():
            return out_file
        dset_in = xr.open_mfdataset([str(inp_file)], combine='by_coords').persist()
        attrs = dset_in.attrs
        dset_out = dset_in.assign_coords({'time': icon2datetime(dset_in.time)})
        if avg_period:
            dset_out = dset_out.resample(time=avg_period).mean()
        for data_var in dset_in.data_vars:
            dset_out[data_var].attrs = dset_in[data_var].attrs
        dset_out.attrs = dset_in.attrs
        write_future = dset_out.to_netcdf(out_file,
                                          engine='h5netcdf',
                                          mode='w',
                                          compute=False).compute()
        return str(out_file)




