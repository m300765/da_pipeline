"""Collection of methods to define and setup the data analysis pipeline."""

from contextlib import contextmanager
from datetime import datetime
from distutils.util import strtobool
from functools import partial
import multiprocessing as mp
import os
from pkg_resources import parse_version
from pathlib import Path
import re
import shlex
from subprocess import CalledProcessError, run, PIPE
import sys
import warnings
import time

from distributed import Client, progress, wait
import humanize
from git import Repo
import papermill as pm
import toml
import xarray as xr

from da_pipeline.utils import Dask_client as da_client
from da_pipeline._run import parse_config, NB_FILE
from da_pipeline.swift import upload
from da_pipeline.ReduceFactory import (SingleLevelWriter, MultiLevelWriter)

warnings.filterwarnings("ignore")
Dask_client = partial(da_client, print_info=True)

TIMEOUT = 24 # Timeout in hours

def run_3d_reduction(config, project_name, results, cdo, addr=None):
    """Apply 3d data reduction.

    Parameters:
    ==========

        config : dict
            Toml based configuration that controls the data pipeline
        project_name: str
            Name of the project the experiment belongs to
        addr : str
            Address of a pre defined task scheduler, which can be used
            instead of creating a new scheduler. This is mainly for
            developing and debugging purpose

    """
    results['run_3d_reduction'] = 1
    slurm_preconf = config['slurm']['run_3d_reduction']
    cluster_config = dict(
            queue=slurm_preconf.get('queue', 'gpu'),
            cores=slurm_preconf.get('cores', 72),
            walltime=slurm_preconf.get('walltime', '5:30:00'),
            memory=slurm_preconf.get('memory', 200),
            workers=slurm_preconf.get('workers', 2),
            account=config['slurm']['account'],
            scheduler_address=addr,
            job_extra=slurm_preconf.get('job_extra', ['--begin=now'])
            )
    dryrun = config['general']['dryrun']
    config = config['reduction']
    exp_name = config.pop('exp_name')
    with Dask_client(cluster_config, f'{project_name}_3Dreduce') as da_client:
        inp_path = Path(config['path'])
        out_path = Path(config['out_dir'])
        config.setdefault('start', None)
        config.setdefault('end', None)
        glob_pattern = config['glob_pattern_3d']
        config.setdefault('vgrid_file', None)
        config.setdefault('grid_file', None)
        target_res = config['target_res']
        max_height = config['max_height']
        min_height = config['min_height']
        if not isinstance(glob_pattern, (list, set, tuple)):
            glob_pattern = glob_pattern,
        if dryrun:
            results['run_3d_reduction'] = 0
            return
        with MultiLevelWriter(inp_path,
                              out_path / f"{config['model_type']}3d",
                              exp_name,
                              glob_pattern,
                              cdo=cdo,
                              **config) as Run3d:
            _ = Run3d.apply()
        results['run_3d_reduction'] = 0

def run_2d_reduction(config, project_name, results, cdo, addr=None):
    """Apply 2d data reduction.

    Parameters:
    ==========

        config : dict
            Toml based configuration that controls the data pipeline
        project_name: str
            Name of the project the experiment belongs to
        addr : str
            Address of a pre defined task scheduler, which can be used
            instead of creating a new scheduler. This is mainly for
            developing and debugging purpose

    """

    results['run_2d_reduction'] = 1
    slurm_preconf = config['slurm']['run_2d_reduction']
    cluster_config = dict(
            queue=slurm_preconf.get('queue', 'prepost'),
            cores=slurm_preconf.get('cores', 42),
            walltime=slurm_preconf.get('walltime', '8:00:00'),
            memory=slurm_preconf.get('memory', 140),
            workers=slurm_preconf.get('workers', 4),
            account=config['slurm']['account'],
            scheduler_address=addr,
            job_extra=slurm_preconf.get('job_extra', ['--begin=now'])
            )
    dryrun = config['general']['dryrun']
    config = config['reduction']
    exp_name = config.pop('exp_name')
    with Dask_client(cluster_config, f'{project_name}_2Dreduce') as da_client:
        inp_path = Path(config['path'])
        out_path = Path(config['out_dir'])
        config.setdefault('start', None)
        config.setdefault('end', None)
        glob_pattern = config['glob_pattern_2d']
        target_res = config['target_res']
        if not isinstance(glob_pattern, (list, set, tuple)):
            glob_pattern = glob_pattern,
        if dryrun:
            results['run_2d_reduction'] = 0
            return
        with SingleLevelWriter(inp_path,
                              out_path / f"{config['model_type']}2d",
                              exp_name,
                              glob_pattern,
                              cdo=cdo,
                              **config) as Run2d:
            _ = Run2d.apply()
        results['run_2d_reduction'] = 0

def clone_frontend(config, project_name, results, *args, **kwargs):
    """Clone the javascript front end repository.

    Parameters:
    ==========

        config : dict
            Toml based configuration that controls the data pipeline
        project_name: str
            Name of the project the experiment belongs to

    """

    results['clone_frontend'] = 1
    clone_dir = Path(config['reduction']['out_dir']) / f'{project_name}_html'
    repo_url = 'https://gitlab.dkrz.de/m300765/da_pipeline_frontend.git'
    if clone_dir.is_dir():
        print('Directory exists, skipping cloning the front end repository')
        results['clone_frontend'] = 0
        return
    if not config['general']['dryrun']:
        Repo.clone_from(repo_url, str(clone_dir))
    results['clone_frontend'] = 1

def run_notebook(configfile, addr=None, nb_file=None, cdo='cdo', freva_path=''):
    """Run and visualisation notebook in batch mode using papermill.

    Parameters:
    ==========

        configfile: str, pathlib.Path
            Filename of the toml config file
        addr : str
            Address of a pre defined task scheduler, which can be used
            instead of creating a new scheduler. This is mainly for
            developing and debugging purpose
        nb_file : str, pathlib.Path
            Path to the notebook that defines the data visualisation

    """
    with Path(configfile).open() as f:
        config = toml.load(f)

    config['general'].setdefault('dryrun', 'False')
    dryrun = bool(strtobool(config['general']['dryrun']))
    slurm_preconf = config['slurm']['run_notebook']
    cluster_config = dict(
            queue=slurm_preconf.get('queue', 'gpu'),
            cores=slurm_preconf.get('cores', 72),
            walltime=slurm_preconf.get('walltime', '5:30:00'),
            memory=slurm_preconf.get('memory', 500),
            workers=slurm_preconf.get('workers', 2),
            account=config['slurm']['account'],
            scheduler_address=addr,
            )
    project_name = config['general']['project_name']
    exp_name = config['reduction']['exp_name']
    nb_file = nb_file or NB_FILE
    out_dir = Path(config['reduction']['out_dir']) / f'{project_name}_html'
    out_path = Path('~').expanduser().absolute() / 'da_pipeline' / 'notebooks'
    out_path.mkdir(exist_ok=True, parents=True)
    out_path /= f'PlotData_{project_name}_{exp_name}_papermill.ipynb'
    cdo_bin = Path(sys.exec_prefix) / 'bin' / 'cdo'
    with Dask_client(cluster_config, f'{project_name}_papermill') as da_client:
        schedule, info = da_client._get_scheduler_info()
        if dryrun:
            return 0
        pm.execute_notebook(str(Path(nb_file).expanduser().absolute()),
                            str(out_path),
                            parameters=dict(sim_config_file=str(configfile),
                                            scheduler_address=schedule.address,
                                            cdo=str(cdo_bin.absolute()),
                                            freva_path=freva_path)
                            )
        print(f'Notebook was successfully in {out_path} created.')
        return 0
    return 1

def run_upload(config, freva_path=''):
    """Upload the plots created by the pipeline to the swift container.

    Parameters:
    ==========
        config: dict
            Toml based configuration that controls the data pipeline
    """

    project_name = config['general']['project_name']
    upload_dir = Path(config['reduction']['out_dir']) / f'{project_name}_html'
    container = config['swift']['container']
    account = config['swift']['account']
    container_path = Path(container) / f'{project_name}_html'
    try:
        container_url = upload(account, upload_dir, container)
    except:
        raise RuntimeError('Upload to failed, try uploading to {container_path} manually')
    print(f'Upload to swift container {container_path} successful')
    if freva_path:
        with (Path(freva_path) / 'aa_container-url.html').open('w') as f:
            f.write(f'''
<!DOCTYPE html>
<html>
    <head></head>
    <body>
        <h3>Find the entire overview page on the <a href="{container_url}" target=_blank>swift cloud storage</a>. </h3>
    </body>
</html>
''')

def get_steps(config):
    """Define the pipeline steps."""

    steps = config.get('steps', ['run_2d_reduction', 'run_3d_reduction'])
    mp_steps = {}
    visualise = False
    for step in steps:
        if '2d' in step.lower():
            mp_steps[step]= run_2d_reduction
        elif '3d' in step.lower():
            mp_steps[step] = run_3d_reduction
        if 'notebook' in step.lower() or 'visual' in step.lower():
            visualise = True
    mp_steps['clone_frontend'] = clone_frontend
    return mp_steps, visualise

def get_cdo_version(minversion='1.9.9'):
    """Get the path to the first cdo version that satisfiy a min version."""

    paths = os.environ['PATH'].split(':')
    paths.insert(0, Path(sys.exec_prefix) / 'bin')
    for path in paths:
        try:
            for cdo_bin in Path(path).rglob('*cdo'):
                res = run(shlex.split(f'{cdo_bin} -V'), stdout=PIPE, stderr=PIPE)
                stderr = res.stderr.decode().split('\n')
                version_str = '.'.join(re.findall("[-\d]+", stderr[0]))
                if parse_version(version_str) >= parse_version(minversion):
                    return str(cdo_bin)
        except (PermissionError, CalledProcessError, IndexError):
            pass
    raise ValueError(f'No cdo version >= {minversion} found')

def setup_pipeline(config=None, config_file=None,
                   address=None, notebook=None, **kwargs):
    """Define and apply the data analysis pipeline.

    Parameters:
    ==========
        config: dict
            Toml based configuration that controls the data pipeline
        config_file: str, pathlib.Path
            Filename of the toml config file
        scheduler_addr : str, default: None
            Address of a pre defined task scheduler, which can be used
            instead of creating a new scheduler. This is mainly for
            developing and debugging purpose
    """

    if address is None:
        address = 3 * [None]
    start_time = datetime.now()
    config['general'].setdefault('dryrun', 'False')
    config['general']['dryrun'] = bool(strtobool(config['general']['dryrun']))
    proj_name = config['general']['project_name']
    exp_name = config['reduction']['exp_name']
    reduction_tasks, is_alive = [], []
    steps, visualise = get_steps(config['general'])
    results = mp.Manager().dict()
    cdo = get_cdo_version(minversion='1.9.9')
    start = time.time()
    timeout = TIMEOUT * 60**2
    for nn, (task_name, task) in enumerate(steps.items()):
        args = config, proj_name, results, cdo, address[nn]
        reduction_tasks.append(mp.Process(target=task, args=args))
        reduction_tasks[-1].start()
        is_alive.append(True)
    while time.time() - start <= timeout:
        for n, task in enumerate(reduction_tasks):
            is_alive[n] = task.is_alive()
        if any(is_alive):
            time.sleep(5)
        else:
            break
    else:
        for task in reduction_tasks:
            task.terminate()
        raise RuntimeError('Timeout has occured; terminated running prcesses')
    _ = [task.join() for task in reduction_tasks]
    exit = [task.exitcode for task in reduction_tasks]
    if len(results) != len(steps):
        raise RuntimeError('Something went wrong setting up the pipeline')
    for step, return_val in results.items():
        if  return_val != 0:
            raise RuntimeError(f'{step} has failed.')
    if visualise:
        vs_res = run_notebook(config_file, address[-1], notebook, cdo=cdo,
                              freva_path=str(kwargs.get('freva_outdir', '')))
        if vs_res != 0:
            raise RuntimeError('Visualisation failed')
        run_upload(config, freva_path=str(kwargs.get('freva_outdir', '')))
    end_time = datetime.now()
    end_str = end_time.strftime('%_d. %b %Y %H:%M')
    time_delta = end_time - start_time
    within = humanize.naturaldelta(time_delta)
    print(f'All pipeline jobs finished at {end_str} (within {within})')


if __name__ == '__main__':
    setup_pipeline(**parse_config(sys.argv))
