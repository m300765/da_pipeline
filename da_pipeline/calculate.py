"""Collection of useful methods to calculate various variables."""
import xarray as xr


def _calc_net_rad(dset, varnames, components):
    """Helper function to calculate radiation."""

    data_vars = {varn: dset[varn] for varn in varnames}
    try:
        net_sw_toa = dset['rsdt'] - dset['rsut']
        net_lw_toa = -dset['rlut']
        net_sw_surf = dset['rsds'] - dset['rsus']
        net_lw_surf = dset['rlds'] - dset['rlus']
    except KeyError:
        net_sw_toa = dset['nswrf_t']
        net_lw_toa = dset['nlwrf_t']
        net_sw_surf = dset['nswrf_s']
        net_lw_surf = dset['nlwrf_s']

    net_toa = net_sw_toa + net_lw_toa
    net_surf = net_sw_surf + net_lw_surf
    net_surf_energy = net_surf + dset['hfls'] + dset['hfss']
    new_data = {'net_surf_energy': net_surf_energy,
                 'net_toa': net_toa, 'net_surf': net_surf}
    for name, data in new_data.items():
        data_vars[name] = data
    if components:
        new_data = {'nswrf_t': net_sw_toa, 'nlwrf_t': net_lw_toa,
                    'nswrf_s': net_sw_surf, 'nlwrf_s': net_lw_surf}
        for name, data in new_data.items():
            data_vars[name] = data
    return xr.Dataset(data_vars=data_vars)


def net_radiation(dset, varnames=None, components=False):
    """Calculate net ratdiation.

    Parameters:
    ==========
        dset: xarray.Dataset
            Input dataset
        varnames: list, tuple, default: None
            Optional subset of variable names that will be returned,
            by default all input variables will be taken.
        components: boolean, default: False
            Do all include the components that contributing to the total
            radiation budgets.
    Returns:
    ========
        xarray.Dataset
    """

    varnames = varnames or list(dset.data_vars)
    if 'exp' in dset.coords:
        exps = {}
        for exp in dset.coords['exp'].values:
            exps[exp] = _calc_net_rad(dset.sel({'exp': exp}),
                                      varnames, components)
    else:
        return _calc_net_rad(dset, varnames, components)
    return xr.concat([data for data in exps.values()],
                     dim='exp').assign_coords({'exp': dset.coords['exp']})
