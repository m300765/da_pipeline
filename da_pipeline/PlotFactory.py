"""Collection of matplotlib plot functions."""

from copy import deepcopy
from getpass import getuser
from pathlib import Path
from tempfile import NamedTemporaryFile
import warnings
import re

warnings.filterwarnings('ignore')

from cartopy import crs as ccrs
from cdo import Cdo
import colorlover as cl
from matplotlib import gridspec, pyplot as plt
import numpy as np
import pandas as pd
import xarray as xr

from matplotlib.axes._axes import _log as matplotlib_axes_logger
import seaborn as sns

sns.set_context('paper')
sns.set(style="ticks")
sns.set_palette('colorblind')
matplotlib_axes_logger.setLevel('ERROR')
col_pal = sns.color_palette("colorblind")


SCRATCH_DIR = f'/scratch/{getuser()[0]}/{getuser()}'


class DiffPlotter:
    """Plot Factory to display different Maps in a subplot."""

    land_name = 'land'
    sea_name = 'sea'

    @staticmethod
    def _plot(dset, im, ax, vmin, vmax, cmap, transform):
        """Plot one map to a given axis."""

        try:
            im.remove()
        except (IndexError, AttributeError, ValueError):
            pass
        return dset.plot(ax=ax, robust=True, add_labels=False,
                                add_colorbar=False, vmin=vmin, vmax=vmax,
                                transform=transform, cmap=cmap)
    @staticmethod
    def _minmax(cmin, cmax, dmin, dmax, cmap, cmap_diff):
        """Get minmax values."""

        vmin = cmin, cmin, dmin
        vmax = cmax, cmax, dmax
        cmap = cmap, cmap, cmap_diff
        return vmin, vmax, cmap

    def _add_colorbars(self):
        """Add colorbars to a plot."""

        caxs, cbars = [], []
        cbar_gs = gridspec.GridSpecFromSubplotSpec(1,
                                                   2,
                                                   subplot_spec=self._gs[-1,0],
                                                   wspace=3)
        for i in range(min(len(self.im_axes), 2)):
            caxs.append(self.fig.add_subplot(cbar_gs[0, i]))
            cbars.append(self.fig.colorbar(self.ims[-i],
                                           cax=caxs[-1],
                                           extend='both',
                                           aspect=.5))
        self._cbar_label = caxs[0].text(-0.8,
                                        0.5,
                                        self.cbar_label,
                                        horizontalalignment='center',
                                       rotation='vertical',
                                       verticalalignment='center',
                                       transform=caxs[0].transAxes)
        return cbars

    def _add_axes(self, nrow, ncol):
        """Add a new axis to a plot."""
        ax_gs = gridspec.GridSpecFromSubplotSpec(1,
                                                 1,
                                                 subplot_spec=self._gs[nrow, ncol],
                                                 wspace=2.5)
        ax = self.fig.add_subplot(ax_gs[0, 0])
        ax.tick_params(bottom=False,
                       labelbottom=False,
                       left=False,
                       labelleft=False)
        ax.axis('off')
        return ax

    @staticmethod
    def _fldmean(data, mask=1):
        """Calcualate field mean."""

        dims = list(data.dims)
        if 'cell' in dims or 'ncells' in dims or 'ncell' in dims:
            for d in ('cell', 'ncells', 'ncell'):
                if d in dims:
                    return data.mean(dim=d)
        return RunAverager._weighted(data, ('lat', 'lon'), mask)

    @staticmethod
    def _create_table(datasets, lsm, landname='land',
                      seaname='sea', extent=None):
        """Create a table containing average values over land/ocean."""

        if extent is not None:
            dset = dset.sel(extent)
            lsm = lsm.sel(extent)
        table = {'Total': [], 'Land': [], 'Ocean': []}
        for dset in datasets:
            table['Total'].append(DiffPlotter._fldmean(dset, 1))
            table['Land'].append(DiffPlotter._fldmean(dset, lsm[landname].data))
            table['Ocean'].append(DiffPlotter._fldmean(dset, lsm[seaname].data))
        if len(datasets) > 1:
            for surf_type in table.keys():
                diff = table[surf_type][0] - table[surf_type][1]
                table[surf_type].append(
                        diff/(table[surf_type][0]+0.0000001) * 100
                        )
        return np.r_[[table['Total']], [table['Ocean']], [table['Land']]]

    def _set_table(self, table_data):
        """Set the data of a already created table."""

        labels = list(self.labels)
        if len(labels) > 1:
            labels[-1] = 'Diff [%]'
        rows, cols = ['Total', 'Ocean', 'Land'], labels
        table_data = np.array([f'{t:>12.02E}' \
            for t in table_data.ravel()]).reshape(table_data.shape)
        try:
            self.table.remove()
        except (AttributeError, ValueError):
            pass
        if len(self.im_axes) > 1:
            loc = 'bottom'
        else:
            loc = 'lower left'
        self.table = self.table_ax.table(cellText=table_data,
                                         rowLabels=rows,
                                         colLabels=cols, loc=loc)
        self.table.auto_set_font_size(False)
        self.table.set_fontsize(7)
        self.table.scale(1.5, 1.5)

    def set_cbar_label(self):
        """Set a new colorbar label."""

        try:
            self._cbar_label.set_text(self.cbar_label)
        except AttributeError:
            pass

    def _create_plotgrid(self, projection):
        """Create the plot layout."""
        ncols = 6 if not self.zone_avg else 8
        nrows = min(len(self.labels), 2)
        if self.timeseries is True:
            ncols += 1
        self._gs = gridspec.GridSpec(nrows, ncols)
        if len(self.labels) > 1:
            self.labels += (f'Diff ({self.labels[0]} - {self.labels[1]})',)
            self.im_axes.append(self.fig.add_subplot(self._gs[0, 0:3],
                                                     projection=projection))
            self.im_axes.append(self.fig.add_subplot(self._gs[0, 3:6],
                                                     projection=projection))
            self.im_axes.append(self.fig.add_subplot(self._gs[1, 1:5],
                                                     projection=projection))
            if self.zone_avg:
                for i in range(2):
                    self.za_axes.append(self.fig.add_subplot(self._gs[i, -1:]))
        else:
            self.im_axes.append(self.fig.add_subplot(self._gs[0, 1:6],
                                                     projection=projection))
            if self.zone_avg:
                self.za_axes.append(self.fig.add_subplot(self._gs[0, -1:]))
        if self.zone_avg:
            self._plot_zone_avg(None)
        if self.timeseries is True:
            self.ts_axes.append(self._gs[-1, :])

    def _plot_zone_avg(self, *dsets):
        """Plot a lineplot for zonal average next to  the map."""

        try:
            self.legend.remove()
        except AttributeError:
            pass
        try:
            line_data = [np.array([dset.mean(dim='lon').values,
                                   dset.lat.values]) for dset in dsets]
        except AttributeError:
            line_data = [np.array([np.random.random_sample(30),
                                   np.linspace(-90, 90, 30)])
                                   for i in self.labels]
        line_data = [line_data[i:i+2] for i in range(0, len(line_data), 2)]
        nplot = 0
        self.lines = []
        for nn, ax in enumerate(self.za_axes):
            ax.clear()
            for ii, l_data in enumerate(line_data[nn]):
                self.lines.append(ax.plot(l_data[0],
                                          l_data[1],
                                          label=self.labels[nplot],
                                          lw=2,
                                          color=col_pal[nplot]))
                nplot += 1
            self.legend = self.fig.legend(loc=1, title='Zonal Avg.')

    def _setup_plot(self, tlabel, extent, projection, **kwargs):
        """Setup the initial plot."""

        self.fig = plt.figure(**kwargs)
        self._create_plotgrid(projection)
        for n, ax in enumerate(self.im_axes):
            if extent is not None:
                ax.set_extent(extent)
            ax.tick_params(bottom=False, labelbottom=False, left=False, labelleft=False)
            ax.coastlines(resolution=self.resolution)
            ax.set_title(self.labels[n])
            self.ims.append(ax.imshow(np.zeros([5, 5]), transform=self.transform))
        if self.cbars:
            self.cbars = self._add_colorbars()
        else:
            self.cbars = [None]
            self._cbar_label = None
        self.table_ax = self._add_axes(0, -1)
        self.label_ax = self._add_axes(-1, -1)
        self.text = self.label_ax.text(0.99, 0.25, tlabel, horizontalalignment='right',
                                       verticalalignment='top', transform=self.label_ax.transAxes)
        self.fig.subplots_adjust(bottom=0.05, top=0.95, left=0.05, right=0.98)
        self.title = self.fig.suptitle('')
        self.set_cmaps()
        self.set_clevels()
        self.text.set_text(tlabel)


    def __init__(self, labels, figsize=(9, 7), transform=ccrs.PlateCarree(),
                 projection=ccrs.Mollweide(50), cbar_label='Net Radiation [W/m²]',
                 extent=None, correct_clt=False, shift=False, cbar=True,
                 timeseries=False, zone_avg=False, lsm_extent=None, **kwargs):
        """Create an new instance of a map plot grid that can be reused.

        If more than two labels are given two maps and a difference plot
        will be created.

        Parameters:
        ==========
            lables: tuple
                Tuple of lables for the subplots
            figsize: tuple, default: (9, 7)
                Size of the plot in inches
            transform: cartopy.crs, default: cartopy.crs.PlateCarre()
                Transformation that is applied to the data
            projection: cartopy.crs, default: cartopy.crs.Mollweide(50)
                Map projection
            cbar_label: str, default: Net Radiation
                color bar label
            extent: str, default: None
                extent of the colorbar, (both, lower, upper, none)
            shift: bool, default: False
                shift data by 180 degree
            cbar: bool, default: True
                display colorbar
            timeseries: bool, default: False
                display a time series underneath the maps
            zon_avg: bool, default: False
                display a zonal average plot next to the maps
        """

        self.cmin = kwargs.get('cmin', -150)
        self.cmax = kwargs.get('cmax', 150)
        self.dmin = kwargs.get('dmin', -150)
        self.dmax = kwargs.get('dmax', 150)
        self.cmap = kwargs.get('cmap', 'RdYlBu_r')
        self.resolution = kwargs.get('resolution', '110m')
        self.cmap_diff = kwargs.get('cmap_diff', 'RdBu_r')
        self.im_axes = []
        self.ims = []
        self.cbars = cbar
        self.ts_axes = []
        self.za_axes = []
        self.lines = []
        self.legends = None
        self.lsm_extent = lsm_extent

        #self.cmin = kwargs.get('cmin', self.cmin)
        #self.cmax = kwargs.get('cmax', self.cmax)
        #self.dmax = kwargs.get('dmax', self.dmax)
        #self.dmin = kwargs.get('dmin', self.dmin)
        #self.cmap = kwargs.get('cmap', self.cmap)
        tstep = kwargs.get('label', 'Init')
        self.zone_avg = zone_avg
        self.cmap_diff = kwargs.get('cmap_diff', self.cmap_diff)
        self.shift = shift
        self.correct_clt = correct_clt
        self.transform = transform
        self.projection = projection
        self.timeseries = timeseries
        if isinstance(labels, str):
            labels = labels, 
        self.labels = tuple(labels)
        self.cbar_label = f'{cbar_label}'
        self._setup_plot(tstep, extent, projection, figsize=figsize)

    def shiftdata(self, lonsin, datain=None, lon_0=None, fix_wrap_around=True):
        """
        Shift longitudes (and optionally data) so that they match map projection region.
        Only valid for cylindrical/pseudo-cylindrical global projections and data
        on regular lat/lon grids. longitudes and data can be 1-d or 2-d, if 2-d
        it is assumed longitudes are 2nd (rightmost) dimension.

        .. tabularcolumns:: |l|L|

        ==============   ====================================================
        Arguments        Description
        ==============   ====================================================
        lonsin           original 1-d or 2-d longitudes.
        ==============   ====================================================

        .. tabularcolumns:: |l|L|

        ==============   ====================================================
        Keywords         Description
        ==============   ====================================================
        datain           original 1-d or 2-d data. Default None.
        lon_0            center of map projection region. Defaut None,
                         given by current map projection.
        fix_wrap_around  if True reindex (if required) longitudes (and data) to
                         avoid jumps caused by remapping of longitudes of
                         points from outside of the [lon_0-180, lon_0+180]
                         interval back into the interval.
                         If False do not reindex longitudes and data, but do
                         make sure that longitudes are in the
                         [lon_0-180, lon_0+180] range.
        ==============   ====================================================

        if datain given, returns ``dataout,lonsout`` (data and longitudes shifted to fit in interval
        [lon_0-180,lon_0+180]), otherwise just returns longitudes.  If
        transformed longitudes lie outside map projection region, data is
        masked and longitudes are set to 1.e30.
        """
        try:
            projparams = self.projection.proj4_params
        except AttributeError:
            try:
                projparams = self.projection.projparams
            except AttributeError:
                porjparams = {}
        if lon_0 is None and 'lon_0' not in projparams:
            msg='lon_0 keyword must be provided'
            raise ValueError(msg)
        lonsin = np.asarray(lonsin)
        if lonsin.ndim not in [1,2]:
            raise ValueError('1-d or 2-d longitudes required')
        if datain is not None:
            # if it's a masked array, leave it alone.
            if not np.ma.isMA(datain): datain = np.asarray(datain)
            if datain.ndim not in [1,2]:
                raise ValueError('1-d or 2-d data required')
        if lon_0 is None:
            lon_0 = projparams['lon_0']
        # 2-d data.
        if lonsin.ndim == 2:
            nlats = lonsin.shape[0]
            nlons = lonsin.shape[1]
            lonsin1 = lonsin[0,:]
            lonsin1 = np.where(lonsin1 > lon_0+180, lonsin1-360 ,lonsin1)
            lonsin1 = np.where(lonsin1 < lon_0-180, lonsin1+360 ,lonsin1)
            if nlons > 1:
                londiff = np.abs(lonsin1[0:-1]-lonsin1[1:])
                londiff_sort = np.sort(londiff)
                thresh = 360.-londiff_sort[-2] if nlons > 2 else 360.-londiff_sort[-1]
                itemindex = nlons-np.where(londiff>=thresh)[0]
            else:
                lonsin[0, :] = lonsin1
                itemindex = 0

            # if no shift necessary, itemindex will be
            # empty, so don't do anything
            if fix_wrap_around and itemindex:
                # check to see if cyclic (wraparound) point included
                # if so, remove it.
                if np.abs(lonsin1[0]-lonsin1[-1]) < 1.e-4:
                    hascyclic = True
                    lonsin_save = lonsin.copy()
                    lonsin = lonsin[:,1:]
                    if datain is not None:
                       datain_save = datain.copy()
                       datain = datain[:,1:]
                else:
                    hascyclic = False
                lonsin = np.where(lonsin > lon_0+180, lonsin-360 ,lonsin)
                lonsin = np.where(lonsin < lon_0-180, lonsin+360 ,lonsin)
                lonsin = np.roll(lonsin,itemindex-1,axis=1)
                if datain is not None:
                    # np.roll works on ndarrays and on masked arrays
                    datain = np.roll(datain,itemindex-1,axis=1)
                # add cyclic point back at beginning.
                if hascyclic:
                    lonsin_save[:,1:] = lonsin
                    lonsin_save[:,0] = lonsin[:,-1]-360.
                    lonsin = lonsin_save
                    if datain is not None:
                        datain_save[:,1:] = datain
                        datain_save[:,0] = datain[:,-1]
                        datain = datain_save

        # 1-d data.
        elif lonsin.ndim == 1:
            nlons = len(lonsin)
            lonsin = np.where(lonsin > lon_0+180, lonsin-360 ,lonsin)
            lonsin = np.where(lonsin < lon_0-180, lonsin+360 ,lonsin)

            if nlons > 1:
                londiff = np.abs(lonsin[0:-1]-lonsin[1:])
                londiff_sort = np.sort(londiff)
                thresh = 360.-londiff_sort[-2] if nlons > 2 else 360.0 - londiff_sort[-1]
                itemindex = len(lonsin)-np.where(londiff>=thresh)[0]
            else:
                itemindex = 0

            if fix_wrap_around and itemindex:
                # check to see if cyclic (wraparound) point included
                # if so, remove it.
                if np.abs(lonsin[0]-lonsin[-1]) < 1.e-4:
                    hascyclic = True
                    lonsin_save = lonsin.copy()
                    lonsin = lonsin[1:]
                    if datain is not None:
                        datain_save = datain.copy()
                        datain = datain[1:]
                else:
                    hascyclic = False
                lonsin = np.roll(lonsin,itemindex-1)
                if datain is not None:
                    datain = np.roll(datain,itemindex-1)
                # add cyclic point back at beginning.
                if hascyclic:
                    lonsin_save[1:] = lonsin
                    lonsin_save[0] = lonsin[-1]-360.
                    lonsin = lonsin_save
                    if datain is not None:
                        datain_save[1:] = datain
                        datain_save[0] = datain[-1]
                        datain = datain_save

        # mask points outside
        # map region so they don't wrap back in the domain.
        mask = np.logical_or(lonsin<lon_0-180,lonsin>lon_0+180)
        lonsin = np.where(mask,1.e30,lonsin)
        if datain is not None and mask.any():
            datain = np.ma.masked_where(mask, datain)

        if datain is not None:
            return lonsin, datain
        else:
            return lonsin

    def set_title(self, text):
        """Set the title of the plot."""

        self.title.set_text(text)

    def set_clevels(self):
        """Set new display limits of the plot."""

        for im in self.ims[:1]:
            im.set_clim(self.cmin, self.cmax)
        try:
            self.dmax = max(np.fabs(self.dmin), np.fabs(self.dmax))
            self.dmin = -self.dmax
            if len(self.ims) > 1:
                self.ims[-1].set_clim(self.dmin, self.dmax)
        except (AttributeError, TypeError):
            pass

    def set_cmaps(self):
        """Set a new colormap."""
        for im in self.ims[:1]:
            im.set_cmap(self.cmap)
        if len(self.im_axes) > 1:
            self.ims[-1].set_cmap(self.cmap_diff)

    def plot(self,
             datasets,
             lsm=None,
             out_f=None,
             overwrite=False,
             label='Average',
             dpi=300):
        """Plot the data.

        Parameters:
        ==========
            datasets: tuple of xr.DataArray
                the data that is plotted
            lsm: xr.DataArray, default: None
                land-see mask for calculation of the overview table, if None
                given (default) no table will be created.
            out_f: str, pathlib.Path
                Output file name, if None given (default) the plot wont be
                be saved to file.
            overwrite: bool, default: False
                overwrite existing files
            label: str, default: Average
                Label that is displayed
            dpi: int, default: 300
                Resolution for saving the data
        """

        try:
            out_f = Path(out_f).expanduser().absolute()
            if Path(out_f).exists() and overwrite is False:
                return
        except TypeError:
            pass
        if lsm:
            table_data = self._create_table(tuple(datasets), lsm,
                                            landname=self.land_name,
                                            seaname=self.sea_name)
            self._set_table(table_data)
        if self.shift:
            tmp_data = []
            for i in range(len(datasets)):
                tmp_data.append(deepcopy(datasets[i]))
                tmp_data[-1].lon.values, tmp_data[-1].values = \
                self.shiftdata(tmp_data[-1].lon, datain=tmp_data[-1].values)
            datasets = tmp_data
        if len(datasets) > 1:
            datasets += (datasets[0] - datasets[1].data, )
        vmin, vmax, cmap = self._minmax(self.cmin,
                                        self.cmax,
                                        self.dmin,
                                        self.dmax,
                                        self.cmap,
                                        self.cmap_diff)
        for n, dset in enumerate(datasets):
            if self.correct_clt:
                vmax[n] *= 1.2
            self.ims[n] = self._plot(dset, self.ims[n], self.im_axes[n],
                                     vmin[n], vmax[n], cmap[n], self.transform)
        self.set_clevels()
        self.set_cmaps()
        self.text.set_text(label)
        for i in (0, -1):
            try:
                self.cbars[i].update_normal(self.ims[i])
            except AttributeError:
                pass
        self.set_cbar_label()
        if out_f:
            out_f.parent.mkdir(exist_ok=True, parents=True)
            self.fig.savefig(f'{out_f}',
                             format='png',
                             bbox_inches='tight',
                             dpi=dpi)
        del datasets

    @staticmethod
    def _get_datasets(dsets, remap=True):
        """Return datasets and difference."""

        if remap:
            dsets  = (DiffPlotter._remap(dsets[0]),
                      DiffPlotter._remap(dsets[1]))
        dsets += (dsets[0] - dsets[1].data, )
        return dsets
