"""Collection of utility functions."""

from datetime import datetime, timedelta
import functools
from getpass import getuser
import os
from pathlib import Path
import numpy as np
import shlex
import random
import time
from tempfile import TemporaryDirectory, NamedTemporaryFile
import toml
import threading
from subprocess import CalledProcessError, run, PIPE
import sys

import dask
from dask_jobqueue import SLURMCluster
from distributed import Client, progress
import pandas as pd
from progress.spinner import MoonSpinner
import xarray as xr

from contextlib import suppress
from distributed.diagnostics.progressbar import TextProgressBar
from distributed.diagnostics.progress import format_time

from .DistUtils import MPICluster

SCRATCH_DIR = f'/scratch/{getuser()[0]}/{getuser()}'

SINGLE_QUEUES = ('gpu', 'prepost')

__all__ = ('get_minmax', 'get_filenames', 'icon2datetime', 'Progress',
           'SCRATCH_DIR')

@dask.delayed
def get_minmax(*dsets, ts=None, timevar='time', symmetric=True, dim=None):
    """Get the 1st and 99th percentile of variables in given dataset(s).

    To determine the display range this methods calculates the 1st and 99th
    percentiles of given input datasets. Input can either be 1 or 2 datasets
    if two datasets are given the 1st and 99th percentile of the differences
    is also calculated.

    Parameters:
    ==========
        dsets : xarray.Dataset, tuple
            Input dataset, tuplel of 2 input datasets
        ts : int, default: None
            Timestep that is taken into account, by default all/no timestep
            are/is taken into account.
        timevar: str
            Name of the time dimension, only relevant if 'ts' is set
        symmetric: bool, default: True
            Make the display range symmetric
        dim: str, default: None
            Dimension along the percentiles are calculated
    Returns:
    =======
        min, max, diff_min, diff_min : tuple
    """

    try:
        dset1, dset2 = dsets
        try:
            dset1 = dset1.isel({timevar: ts})
            dset2 = dset2.isel({timevar: ts})
        except (ValueError, TypeError):
            pass
        except IndexError:
            print('Warning, timestep not found')
            return np.nan, np.nan, np.nan, np.nan
        diff = dset1 - dset2.data
        dmin = float(diff.quantile(0.01, dim=dim).min())
        dmax = float(diff.quantile(0.999, dim=dim).max())
        dd = max(np.fabs(dmin), np.fabs(dmax))
        dd1 = dd
        dd2 = -dd
        cmin = min(float(dset1.quantile(.01, dim=dim).min()),
                   float(dset2.quantile(.01, dim=dim).min()))
        cmax = max(float(dset1.quantile(.99, dim=dim).max()),
                   float(dset2.quantile(.99, dim=dim).max()))
    except ValueError:
        try:
            dset1 = dsets[0].isel({timevar: ts})
        except (ValueError, TypeError):
            dset1 = dsets[0]
        except IndexError:
            print('Warning, timestep not found')
            return np.nan, np.nan, None, None
        cmin = float(dset1.quantile(.01, dim=dim).min())
        cmax = float(dset1.quantile(.99, dim=dim).max())
        dd1, dd2 = None, None
    if np.isnan([cmin, cmax]).any() and ts is not None:
        get_minmax(varn,
                   ts+1,
                   *dsets,
                   timevar=timevar,
                   symmetric=symmetric,
                   dim=dim)
    if np.sign(cmin) != 0. and np.sign(cmax) != 0. and symmetric:
        if np.sign(cmin) != np.sign(cmax):
            cmax = max(np.fabs(cmin), np.fabs(cmax))
            cmin = -cmax
    return cmin, cmax, dd1, dd2


def try_repeat(func):
    """Decorator that retries to execute functions."""

    @functools.wraps(func)
    def repeat_wrapper(*args, **kwargs):
        for try_num in range(1, 5):
            try:
                return func(*args, **kwargs)
            except Exception as e:
                time.sleep(random.uniform(1, 5))
                if try_num == 4:
                    raise e
    return repeat_wrapper


class TmpDir:
    """Wrapper class for creating a temporary directory."""

    def __init__(self, prefix, dir=None):
        """Create a temp-dir in a given directory and prefix."""

        self._tmp = TemporaryDirectory(dir=dir or SCRATCH_DIR, prefix=prefix)
        self.name = self._tmp.name

    def close(self):
        """Cleanup the directory."""

        try:
            self._tmp.cleanup()
        except OSError:
            [f.unlink() for f in Path(self.name).rglob('*')]
            Path(self.name).rmdir()


class Progress:
    """Patched version of a dask-progress bar with a label."""

    def __init__(self, *args, label='', **kwargs):
        """Call dask-distributed porgress with a label."""

        TextProgressBar._draw_bar = self._draw_bar
        TextProgressBar.label = label
        kwargs.setdefault('leave', True)
        try:
            progress(*args, **kwargs)
        except Exception as e:
            TextProgressBar.label = ''
            raise e
        TextProgressBar.label = ''

    @staticmethod
    def _draw_bar(cls, remaining, all, **kwargs):
        """Monkey-Patch dask text progress bar to display bar label."""

        frac = (1 - remaining / all) if all else 1.0
        bar = "#" * int(cls.width * frac)
        percent = int(100 * frac)
        elapsed = format_time(cls.elapsed)
        try:
            label = cls.label
        except AttributeError:
            label = ''
        msg = "\r{4}[{0:<{1}}] | {2}% Completed | {3}".format(
            bar, cls.width, percent, elapsed, label
        )
        with suppress(ValueError):
            sys.stdout.write(msg)
            sys.stdout.flush()


class Averager:
    """Collection of methods to create averages."""

    @staticmethod
    def _weighted(data, avg_dims, mask, skipna, *args, **kwargs):
        """Calculate weight average with a given mask."""

        latn = kwargs.get('latname', 'lat')
        lonn = kwargs.get('lonname', 'lon')
        weight = Averager._get_area(data[latn].data, data[lonn].data) * mask
        return (data * weight).sum(avg_dims, skipna=skipna) / np.nansum(weight)

    @staticmethod
    def _get_area(lat, lon):
        """Calculate the area of a gird-cell."""

        R = 6371.  # Earth's radius in km
        dlon = np.deg2rad(np.fabs(lon[0]-lon[1]))
        areas = np.pi / 360 * R**2 * np.fabs(np.diff(
                                             np.sin(np.deg2rad(lat)))) * dlon
        center_lat = lat[1:] - np.fabs(lat[0] - lat[1])/2
        areas = np.interp(lat, center_lat, areas)
        return np.meshgrid(np.ones_like(lon), areas)[-1]

    @staticmethod
    def fldmean(data, land_frac=None, skipna=True):
        """CDO equivalent of field mean.

        Parameters:
        ==========
            data : xr.DataArray, xr.Dataset
                input data
            land_frac: xr.DataArray, dask.array, numpy.array, default: None
                mask that is applied to the data
            skipna: bool, default: True
                drop nan values
        Returns:
        =======
            xr.DataArray, xr.Dataset: field mean
        """

        if land_frac is not None:
            try:
                mask = np.ma.masked_invalid(land_frac.values)
            except AttributeError:
                mask = np.ma.masked_invalid(land_frac)
            mask = dask.array.from_array(mask.round(0))
        else:
            mask = 1
        return Averager._weighted(data, ('lat', 'lon'), mask, skipna)


@dask.delayed
def run_cmd(cmd, path_extra=Path(sys.exec_prefix)/'bin', returnVal=None):
    """Wrapper to run a bash command.

    Parameters:
    ==========
    cmd : str, tuple, list
        bash command that is applied
    path_extra : str, pathlib.Path, default: sys.exec_prefix / 'bin'
        extra PATH environment variables
    returnVal : default: None
        pre defined return Value

    Returns:
    =======
        str: ouput of the command or pre defined return Value
    """

    if isinstance(cmd, (tuple, list, set)):
        cmd = ' '.join(cmd)
    cmd = shlex.split(cmd)
    env_extra = os.environ.copy()
    path = env_extra['PATH'].split(':')
    path[0], path[-1] = path[-1], path[0]
    env_extra['PATH'] =  ':'.join(path) + ':' + str(path_extra)
    for i in range(4):
        status = run(cmd,
                     check=False,
                     stderr=PIPE,
                     stdout=PIPE,
                     env=env_extra)
        out_st = status.stdout.decode('utf-8')
        err_st = status.stderr.decode('utf-8')
        try:
            status.check_returncode()
            return returnVal or out_st
        except Exception as e:
            if i == 3:
                raise CalledProcessError(status.returncode,
                                         f'The following command failed:\n " ".join({cmd})\n\t std err: {err_st}\n\t std out: {out_st}')
        time.sleep(random.uniform(1, 5))


class DummyCluster:
    """Mock cluster."""

    @staticmethod
    def close(*args, **kwargs):
        """Mock close."""
        return


class Dask_client(Client):
    """Wrapper around a dask distributed client."""

    def __exit__(self, type, value, traceback):
        """Overwrite the __exit__ method."""
        try:
            self.close()
        except Exception as e:
            pass
        try:
            self.wrk_cluster.close()
        except Exception as e:
            pass
        self.wrkdir.close()
        if self.print_info:
            print(f'\n{self.job_name} finished.')

    def __init__(self, config, job_name, print_info=False, single_jobs=False, **kwargs):
        """Create a new dask distributed scheduler.

        Parameters:
        ==========
            config: dict
                Configuration to setup a distributed cluster
            job_name: str
                Name of the cluster
            print_info: bool, default: False
                print client is being closed
            single_jobs: Order single jobs only
        """

        self.wrkdir = TmpDir(job_name)
        self.wrk_cluster = DummyCluster
        self.print_info = print_info
        self.job_name = job_name
        cluster_kwargs = {}
        single_jobs = config['queue'].lower() in SINGLE_QUEUES or single_jobs

        try:
            addr = config['scheduler_address']
            cluster_kwargs['address'] = addr
        except KeyError:
            addr = None
        if addr is None and single_jobs:
            self.wrk_cluster = Cluster(config, job_name, self.wrkdir, **kwargs)
            cluster_kwargs['address'] = self.wrk_cluster.scheduler_address
        elif addr is None:
            self.wrk_cluster = mpi_cluster(config, job_name, self.wrkdir, **kwargs)
            cluster_kwargs['scheduler_file'] = self.wrk_cluster.scheduler_file
        super().__init__(**cluster_kwargs)


def mpi_cluster(config, job_name, wrkdir=None, print_info=False):
    """Create and MPICluster instance."""

    wrkdir = wrkdir or TmpDir(job_name)
    if not isinstance(wrkdir, (str, Path)):
        wrkdir = wrkdir.name
    config.setdefault('cores', 0)
    slurm_cfg = dict(job_extra=config.get('job_extra', []),
                     memory=f"{config['memory']}G",
                     walltime=config['walltime'],
                     nworkers=config['workers'],
                     name=job_name,
                     cpus_per_task=config['cores'],
                     workdir=wrkdir
                     )
    if config.get('memory', 0) == 0:
        slurm_cfg['memory'] = '0'
    cluster = MPICluster.slurm(config['account'], config['queue'], **slurm_cfg)
    nitt, max_itt = 0, 4000
    print(f'Waiting for {config["workers"]} workers...', end='', flush=True)
    while (cluster.status != 'R'):
        nitt += 1
        time.sleep(15)
        if nitt >= max_itt:
            print(' failed, aborting')
            cluster.close()
            wrkdir.close()
            sys.exit('No workers could be started, aborting')
    print(f' done.')
    return cluster

class Cluster(SLURMCluster):
    """Wrapper around a SLURMCluster."""



    def _get_num_of_workers(self):

        workers = []
        for addr in self.scheduler_info['workers'].keys():
            ip = ':'.join(addr.split(':')[:-1])
            if ip not in workers:
                workers.append(ip)
        return len(workers)

    def __init__(self, config, job_name, wrkdir=None, print_info=False):
        """Start a slurm cluster for dask distributed scheduler.

        Parameters:
        ==========
            config: dict
                Configuration to setup a distributed cluster
            job_name: str
                Name of the cluster
            wrkdir: str, pathlib.Path, default: None
                Working directory, a new temporary directory is created
                if None is given (default)
            print_info: bool, default: False
                print information when slurm jobs are submitted
        """

        self.wrkdir = wrkdir or TmpDir(job_name)
        log_file = Path(self.wrkdir.name) / 'LOG_mpicluster.%j.o'
        config.setdefault('job_extra',[])
        super().__init__(memory=f"{config['memory']}GiB",
                         cores=config['cores'],
                         project=config['account'],
                         walltime=config['walltime'],
                         queue=config['queue'],
                         processes=1,
                         local_directory=self.wrkdir.name,
                         job_extra=[f'-J {job_name}',
                                    f'-D {self.wrkdir.name}',
                                    f'--output={log_file}',
                                    f'--output={log_file}'
                                    ]+config['job_extra'],
                         interface='ib0')
        self.scale(jobs=config['workers'])
        min_workers = max(1, config['workers'] - 1)
        nitt, max_itt = 0, 4000
        print(f'Waiting for {config["workers"]} workers...', end='', flush=True)
        while (self._get_num_of_workers() < min_workers):
            nitt += 1
            time.sleep(15)
            if nitt >= max_itt:
                print(' failed, aborting')
                self.close()
                self.wrkdir.close()
                sys.exit('No workers could be started, aborting')
        print((f' done. scheduler_address: {self.scheduler_address}'
               f' dash_board: {self.dashboard_link}'))
        if print_info:
            ip, port = self.dashboard_link.partition('/')[-1].partition('/')[-1].partition('/')[0].split(':')
            user = getuser()
            print(f'''You can connect to the dash_board using the following command:
ssh -L {port}:localhost:{port} {getuser()}@mistral.dkrz.de ssh -L {port}:{ip}:{port} -N {ip}

And open the following link in you browser:
http://localhost:{port}/status
''')


def wait_for_workers(min_threads, threads_per_core=2):
    """Waiting for a minimum number of threads for of a distributed client."""

    def is_num_workers_enough():
        try:
            threads = [core*threads_per_core for core in cl.ncores().values()]
        except AttributeError:
            threads = [0]
        total_threads = sum(threads)
        if total_threads >= min_threads:
            return True
        return False

    cl = dask.distributed.client._get_global_client()
    if is_num_workers_enough(): return cl
    with Spinner(f'Waiting for {min_threads} threads ... '):
        while not is_num_workers_enough():
            cl = dask.distributed.client._get_global_client()
            time.sleep(1)
        return cl


class Spinner:
    """Spinning object to display that calculation is ongoing."""

    def stop(self):
        """Stop the spinning thread."""

        try:
            self._thread_stop.set()
        except AttributeError:
            pass

    def _start(self):
        """Start a thread the displays the spinning object."""

        self._thread_stop = threading.Event()
        self._thread.start()

    def __init__(self, txt):
        """Start a MoonSpinner in a separate thread.

        Parameters:
            txt: str
                Text to be displayed next to the MoonSpinner
        """

        self.txt = txt
        self.spinner = MoonSpinner(f'{txt}:  ')
        self._thread_stop = threading.Event()
        self._thread = threading.Thread(target=self._spin)
        self._start()

    def _spin(self):
        """Spin the Moon."""

        while not self._thread_stop.is_set():
            time.sleep(0.5)
            self.spinner.next()
        self.spinner.clearln()
        print(f'{self.txt}:  done')

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.stop()
        return True


def get_filenames(out_dir, exp_name, glob_pattern, start, end, filetype='nc'):
        """Helper function that extracts filenames that ought to be processed.

        Parameters:
        ===========

        exp_dir  :
            Directory of where the experiment data resides
        exp_name :
            Name of the experiment
        glob_pattern :
            Collections of patterns that identify the filenames of interest
        start :
            First time step to process
        end :
            Last time step to process

        Returns:
        =======
            files : list
        """
        out_dir = Path(out_dir)
        if isinstance(glob_pattern, dict):
            glob_pattern = glob_pattern[exp_name]
        if not isinstance(glob_pattern, (list, set, tuple)):
            glob_pattern = glob_pattern,
        start, end = pd.DatetimeIndex([start, end]) # Convert to pandas datetime index
        files = []
        for prefix in glob_pattern:
            if exp_name:
                search_string = f'{exp_name}*{prefix}*.{filetype}'
            else:
                search_string = f'{prefix}*.{filetype}'
            for f in out_dir.rglob(search_string):
                timeStep = pd.DatetimeIndex([str(f.with_suffix('')).split('_')[-1].strip('Z')])[0]
                if timeStep >= start and timeStep <= end:
                    files.append(str(f))
        return sorted(files)


def icon2datetime(icon_dates, start=None):
    """Convert datetime objects in icon format to python datetime objects.

    ::

        time = icon2datetime([20011201.5])

    Parameters:
    ==========

    icon_dates: collection
        Collection of icon date dests

    Returns:
    ========

        dates:  pd.DatetimeIndex
    """

    try:
        icon_dates = icon_dates.values
    except AttributeError:
        pass

    try:
        icon_dates = icon_dates[:]
    except TypeError:
        icon_dates = np.array([icon_dates])

    def convert(icon_date):
        frac_day, date = np.modf(icon_date)
        frac_day *= 60**2 * 24
        return datetime.strptime(str(int(date)), '%Y%m%d')\
                + timedelta(seconds=int(frac_day.round(0)))
    conv = np.vectorize(convert)
    try:
        out = conv(icon_dates)
    except TypeError:
        out = icon_dates
    return pd.DatetimeIndex(out)


@dask.delayed
def _get_enc(dataset, **kwargs):
    """Get encoding to save datasets."""
    enc = {}
    missing_val = kwargs.get('missing_val', -9.99e37)
    for varn in dataset.data_vars:
        enc[varn] = {'_FillValue': missing_val}
    return enc
