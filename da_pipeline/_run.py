"""Entry point script for calling the data analysis pipeline."""

import argparse
import os
from pathlib import Path
from subprocess import CalledProcessError
import sys
from tempfile import NamedTemporaryFile

import toml
from da_pipeline.swift import check


NB_FILE = Path(__file__).parent / 'scripts' / 'PlotData.ipynb'

def gen_jobscript(config, configfile, outfile, args, *,
                  walltime='24:00:00', partition='shared',
                  begin='now'):
    """Generate a jobscript that submits the actual pipeline job."""
    configfile = Path(configfile).expanduser().absolute()
    python = Path(sys.executable).expanduser().absolute()
    return '''#!/usr/bin/env bash

#SBATCH -J run-pipeline
#SBATCH -p {partition}
#SBATCH -A {account}
#SBATCH -n 1
#SBATCH --cpus-per-task=10
#SBATCH --mem=24G
#SBATCH --begin={begin}
#SBATCH -t {walltime}
#SBATCH --output={outfile}
#SBATCH --error={outfile}

JOB_ID=${{SLURM_JOB_ID%;*}}

{python} -m da_pipeline.main {args}

'''.format(partition=partition, begin=begin, walltime=walltime, outfile=outfile,
        python=python, account=config['slurm']['account'], args=' '.join(args))



def parse_config(argv=None):
    """Construct command line argument parser."""

    argp = argparse.ArgumentParser
    ap = argp(prog='run_pipeline',
              description="""Apply data pipeline.""",
              formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    ap.add_argument('config', metavar='config', type=Path,
                    help='Toml file containing the pipeline configuration')
    ap.add_argument('-a', '--address', type=str, default=(None, None, None),
                    nargs=3,
                    help='Give a tcp address to a distributed scheduler')
    ap.add_argument('-nb', '--note-book', '--notebook',
                    type=Path, default=NB_FILE,
                    help='Chose notebook for visualization')
    ap.add_argument('--begin', default='now',
                    help='Choose begin time of the job, only applicable with slurm jobs')
    ap.add_argument('--no_slurm', '--no-slurm', '--noslurm', action='store_true',
                    default=False,
                    help='Do not submit the pipeline jobs via slurm, rather start a background job')
    ap.add_argument('--password', '-pw', type=str, default=None,
                    help='Preset a password for the swift account, '
                         'if None given (default) you will be ask a password.')
    ap.add_argument('--foreground', '-fg', action='store_true', default=False,
                    help='Do not run the pipeline in the background')
    ap.add_argument('--freva_outdir', type=str, default='',
                   help='This pipeline was setup by freva, do not use this flag')
    args = ap.parse_args()
    slurm = args.no_slurm == False
    config_file = args.config.expanduser().absolute()
    config = dict(config_file=config_file,
                  address=args.address,
                  notebook=args.note_book,
                  begin=args.begin,
                  slurm=slurm,
                  fg=args.foreground,
                  password=args.password,
                  freva_outdir=args.freva_outdir)
    with config_file.open('r') as tf:
        config['config'] = toml.load(tf)
        return config

def install_kernel():

    cmd = f'{sys.executable} -m ipykernel install --user --name da_pipeline --display-name=da-pipeline'
    res = os.system(cmd)
    if res != 0:
        raise CalledProcessError(f'Command Failed', cmd)

def main(argv=None):
    """Do system to setup the analysis pipeline."""

    config = parse_config(sys.argv)
    cfg = config['config']
    config_file = config['config_file']
    add = config['address']
    nb_file = config['notebook']
    begin = config['begin']
    slurm = config['slurm']
    password = config['password'] or None
    _ = check(cfg['swift']['account'],
              password=password,
              username=cfg['swift'].get('username', '').strip() or None)
    out_path = Path('~').expanduser().absolute() / 'da_pipeline' / 'output'
    out_path.mkdir(exist_ok=True, parents=True)
    out_file = out_path / f'{cfg["reduction"]["exp_name"]}_pipeline.out'
    num = 1
    while out_file.is_file():
        out_file = out_path / f'{cfg["reduction"]["exp_name"]}_pipeline_{num}.out'
        num += 1
    install_kernel()
    args = sys.argv[1:]
    for nn, arg in enumerate(args):
        if arg.startswith('--begin'):
            args.pop(nn)
    job_script_file = None
    cmd = f'{sys.executable} -m da_pipeline.main {" ".join(sys.argv[1:])}'
    if config['fg']:
        msg = ''
    elif slurm:
        job_script = gen_jobscript(cfg, config_file, out_file, args,
                                   begin=begin, walltime='24:00:00',
                                   partition='shared')
        msg = f'Pipeline was submitted via slurm,\ncheck output in {out_file}'
        with NamedTemporaryFile(prefix='da-pipeline', suffix='.sh') as tf:
            job_script_file = Path(tf.name)
        #job_script_file = Path('.') / 'job_script.sh'
        job_script_file.touch(0o755)
        cmd = f'sbatch {job_script_file}'
        with job_script_file.open('w') as f:
            f.write(job_script)
    else:
        cmd = f'nohup {cmd} &> {out_file} &'
        msg = f'Pipeline was setup in the background,\ncheck output in {out_file}'
    res = os.system(cmd)
    if res != 0:
        raise CalledProcessError(f'Submission of pipeline failed:', cmd)
    print(msg)
    out_file.touch(0o644)
    if job_script_file:
        job_script_file.unlink()


if __name__ == '__main__':
    main()

