
from functools import partial
import concurrent.futures as cf
import multiprocessing as mp
from pathlib import Path
from subprocess import run, PIPE
import shlex
import sys
from tempfile import TemporaryDirectory

import cartopy
from cartopy import crs
import pandas as pd
import pytz
import numpy as np
import xarray as xr

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

#from ..utils import SCRATCH_DIR

class MapCreator:
    """Base Clase that servers as a map factory."""

    def __init__(self, **kwargs):
        """Instanciated a map class with the most important attributes."""

        for key, value in (('color', 'black'),
                           ('resolution', '10m'),
                           ('linewidth', 0.8),
                          ('transform', crs.PlateCarree())):
            kwargs.setdefault(key, value)
        self.color = kwargs.pop('color', 'black')
        self.res = kwargs.pop('resolution', '10m')
        self.linewidth = kwargs.pop('linewidth', 0.8)
        self.transform = kwargs.pop('transform')
        self.figsize = kwargs.pop('figsize', (9, 9))
        self._kwargs = kwargs
        self.fig = plt.figure(figsize=self.figsize)
        self.fig.subplots_adjust(left=0.01, right=0.99, hspace=0, wspace=0, top=1, bottom=0.03)


class GlobalMap(MapCreator):
    """Class for Map objects on a Global Map."""

    def __init__(self, lon, lat, proj, **kwargs):
        """Instanciate a global map using Cartopy.

        Parameters:
        ==========
            lon (xr.DataArray, np.array):
                the Longitude Vector
            lat (xr.DataArray, np.array):
                the Latitude Vector
            proj (str, cartopy.crs):
                The cartopy map projection, if proj is of type str then
                the projection is going to be created using the evaluation
                proj string and the cartopy.crs call
            kwargs: 
                Additional keyword arguments that are passed to the 
                map creator factory
        """

        super().__init__(**kwargs)
        kwargs.setdefault('lonlatbox', None)
        if isinstance(proj, str):
            proj = eval(f'crs.{proj}')
        self.proj = proj
        self.lon = lon
        self.lat = lat
        self.ax = plt.axes(projection=self.proj)
        self.transform = crs.PlateCarree()
        self.ax.coastlines(self.res, linewidth=self.linewidth, color=self.color)
        self.ax.add_feature(cartopy.feature.LAKES, edgecolor=self.color,
                            facecolor='none', linewidth=self.linewidth)
        box = kwargs.pop('lonlatbox')
        xs, ys = np.r_[min(lon), max(lon)], np.r_[min(lat), max(lat)]
        if isinstance(box, (list, tuple, set)):
            if len(box) == 4:
                self.ax.set_extent(box, crs=self.transform)


class RotPole(MapCreator):
    """Class for Map objects on a Regionla Map (Rotated Pole)."""

    earth_radius = 6370000. # Earth radius in [m]
    def __init__(self, rlon, rlat, rot_pole, **kwargs):
        """Instanciate a map with a rotated pole using Cartopy.

        Parameters:
        ==========
            rlon (xr.DataArray, np.array):
                the Longitude Vector
            rlat (xr.DataArray, np.array):
                the Latitude Vector
            rot_pole (dict):
                Dictionary containing information on the rotated pole e.g.
                {grid_north_pole_longitude: 180, grid_north_pole_latitude: 50}
            kwargs:
                Additional keyword arguments that are passed to the 
                map creator factory
        """
        super().__init__(**kwargs)
        kwargs.setdefault('lonlatbox', None)
        self.rlon = rlon
        self.rlat = rlat
        self.pole = rot_pole['grid_north_pole_longitude'], rot_pole['grid_north_pole_latitude']
        self.proj =  crs.RotatedPole(pole_longitude=self.pole[0],
                                     pole_latitude=self.pole[-1],
                                     globe=crs.Globe(semimajor_axis=self.earth_radius,
                                                     semiminor_axis=self.earth_radius))
        self.ax = plt.axes(projection=self.proj)
        self.transform = self.proj
        self.ax.coastlines(self.res, linewidth=self.linewidth, color=self.color)
        self.ax.add_feature(cartopy.feature.LAKES,
                            edgecolor=self.color,
                            facecolor='none',
                            linewidth=self.linewidth)
        box = kwargs.pop('lonlatbox')
        xs, ys = np.r_[min(rlon), max(rlon)], np.r_[min(rlat), max(rlat)]
        if box:
            if len(box) == 4:
                xs, ys, zs = self.proj.transform_points(slef.proj,
                                                        np.r_[box[0],
                                                              box[1]],
                                                              np.r_[box[2],
                                                                    box[3]]).T
        self.ax.set_xlim(xs)
        self.ax.set_ylim(ys)

class Plotter:
    """General Plotter Class providing various plot methods."""

    tz = 'utc'
    dirname = '.'
    _defaults = {
        'mapping_method': 'from_global_grid',
        'plotting_method': 'pcolormesh'
    }
    def __init__(self, mapobj, lon, lat, dset, **kwargs):
        """Instanciate the Plot object.

        Parameters:
        ===========
            mapobj: (MapCreator)
                MapCreator object where the data gets plotted onto
            lon (xr.DataArray, numpy.array):
                the Longitude Vector
            lat (xr.DataArray, numpy.array):
                the Latitude Vector
            dset (xr.DataArray, numpy.array):
                The data to be plotted
        """
        try:
            long_name = dset.attrs['long_name']
            units = dset.attrs['units']
        except (KeyError, AttributeError):
            long_name = units = ''
        cbar_label = f'{long_name} [{units}]'
        for key, v in dict(fontsize=18, cbar_label=cbar_label, title='').items():
            mapobj._kwargs.setdefault(key, v)
        self.proj = mapobj.proj
        self.lon = lon
        self.lat = lat
        self.cbar = None
        self.im = None
        self.fig = mapobj.fig
        self.ax = mapobj.ax
        self.transform = mapobj.transform
        self._cbar_label = mapobj._kwargs.pop('cbar_label')
        self.fontsize = mapobj._kwargs.pop('fontsize')
        self.title = mapobj._kwargs.pop('title')
        self.ax.set_title(self.title, fontsize=self.fontsize)
        self._kwargs = mapobj._kwargs

    @classmethod
    def from_rotated_grid(cls, rlon, rlat, dset, rot_pole, *args, **kwargs):
        """Create a Plot object for plotting on a map with a rotated pole.
        Parameters:
        ==========
            rlon (xr.DataArray, np.array):
                the Longitude Vector
            rlat (xr.DataArray, np.array):
                the Latitude Vector
            dset (xr.DataArray, numpy.array):
                The data to be plotted
            rot_pole (dict):
                Dictionary containing information on the rotated pole e.g.
                {grid_north_pole_longitude: 180, grid_north_pole_latitude: 50}
            kwargs:
                Additional keyword arguments that are passed to the 
                map creator factory

        Returns:
        ========
        Plotter: Instance of the Plot obj for plotting on maps with roated poles
        """

        Map = RotPole(rlon, rlat, rot_pole, **kwargs)
        cls.tz = 'Europe/Berlin'
        return cls(Map, rlon, rlat, dset, **kwargs)

    @classmethod
    def from_global_grid(cls, lon, lat, dset, map_call, *args, **kwargs):
        """Create a Plot object for plotting on a global map.

        Parameters:
        ===========
            lon (xr.DataArray, np.array):
                the Longitude Vector
            lat (xr.DataArray, np.array):
                the Latitude Vector
            dset (xr.DataArray, numpy.array):
                The data to be plotted
            map_call (str, cartopy.crs):
                The cartopy map projection, if proj is of type str then
                the projection is going to be created using the evaluation
                proj string and the cartopy.crs call
            kwargs:
                Additional keyword arguments that are passed to the 
                map creator factory

        Returns:
        ========
        Plotter: Instance of the Plot obj for plotting on global maps
        """

        Map = GlobalMap(lon, lat, map_call, **kwargs)
        cls.tz = 'UTC'
        return cls(Map, lon, lat, dset, **kwargs)

    def pcolormesh(self, data, **kwargs):
        """Plot data on a colormesh.
        
        Parameters:
        ===========
            data : (xr.DataArray, numpy.array)
                2D Data to be plotted
        """
        #kwargs.setdefault('shading', 'nearest')
        #kwargs.setdefault('add_colorbar', False)
        kwargs.setdefault('antialiased', True)
        kwargs.setdefault('transform', self.transform)
        try:
            kwargs.pop('lonlatbox')
        except KeyError:
            pass
        try:
            plot_data = data.values
        except AttributeError:
            plot_data = data
        im = plt.pcolormesh(self.lon, self.lat, plot_data, **kwargs)
        self.ax.set_title(self.title, fontsize=self.fontsize)
        if self.cbar is None:
            cbar_label = self.get_cbar_label(data)
            self.cbar = self.fig.colorbar(im, ax=self.ax, orientation='horizontal', 
                                          shrink=0.74, pad=0.02, extend='both')
            self.cbar.set_label(cbar_label, fontsize=self.fontsize)
        return im

    def get_timestamp(self, dset, fmt=' (%Y/%m/%d %H:%M)'):
        """Construt the string repr. of a timestep from a given xr.DataArray."""
        try:
            time = pd.DatetimeIndex(np.array([dset.time.data]), tz='utc').to_pydatetime()[0]
        except AttributeError:
            return ''
        time_lt = time.astimezone(pytz.timezone(self.tz))
        return time_lt.strftime(fmt)

    def get_cbar_label(self, dset):
        """Construct the label of the colorbar."""
        cbar_label = self._cbar_label
        return self._cbar_label + self.get_timestamp(dset)


def plot_map(dset, lon, lat, *args, dirname=None, **kwargs):
    """Create a plot of an xarray.DtaArray.

    Parameters:
    ===========
        lon (xr.DataArray, np.array):
                the Longitude Vector
        lat (xr.DataArray, np.array):
                the Latitude Vector
        dset (xr.DataArray):
                The data to be plotted
        dirname (str, pathlib.Path):
            The filename of the plot. If you set this to None (default)
            now figure will be saved to file, but rather a plot object
            will be returned for addtional work.
        kwargs: Additional keyword arguments for the Plotting and Mapping
                classes
    """

    for key, value in Plotter._defaults.items():
        kwargs.setdefault(key, value)
    map_method = kwargs.pop('mapping_method')
    plot_method = kwargs.pop('plotting_method')
    plot = getattr(Plotter, map_method)(lon, lat, dset, *args, **kwargs)
    plot.im = getattr(plot, plot_method)(dset,  **plot._kwargs)
    if not dirname:
        return plot
    ts = plot.get_timestamp(dset, fmt='_%Y%m%d_%H%M')
    fn = f'{dset.name}'
    if ts:
        fn += ts
    fn += '.png'
    dirname = Path(dirname)
    fname = (dirname / fn).expanduser().absolute()
    plot.fig.savefig(str(fname), dpi=150, bbox_inches='tight')
    plot.fig.clf()
    plt.close()
    return fname


def _pool_wrapper(first_arg, func=plot_map, args=(), kwargs={}):
    """Wrapper to apply a given function in a mp.Pool."""

    return func(first_arg, *args, **kwargs)




def _create_animation(figdir, outputfile):
    """FFmpeg calls."""
    ffmpeg = Path(sys.exec_prefix) / 'bin' / 'ffmpeg'
    outputfile = Path(outputfile)
    outputfile.parent.mkdir(exist_ok=True, parents=True)
    with TemporaryDirectory(prefix='animate') as td:
        outfile = Path(td) / 'out.mp4'
        if outputfile.suffix == '.mp4':
            outfile = outputfile
        cmd = shlex.split(f"{ffmpeg} -framerate 10 -pattern_type glob -i '{figdir}/*.png' -c:v libx264 -pix_fmt yuv420p -vf 'scale=trunc(iw/2)*2:trunc(ih/2)*2' -y {outfile}")
        res = run(cmd, stdout=PIPE, stderr=PIPE)
        if outputfile.suffix == '.mp4':
            return outputfile
        outputfile = outputfile.with_suffix('.gif')
        cmd = shlex.split(f"{ffmpeg} -y -i {outfile} -vf palettegen {Path(td)/'palette.png'}")
        res = run(cmd, stdout=PIPE, stderr=PIPE)
        cmd= shlex.split(f"{ffmpeg} -y -i {outfile} -i {Path(td)/'palette.png'} -filter_complex paletteuse -r 10 {outputfile}")
        res = run(cmd, stdout=PIPE, stderr=PIPE)
    return outputfile

def animate(lon, lat, dset, outputfile, plot_range, *args, **kwargs):
    """Create an animation of a xarray.DtaArray.

    Parameters:
    ===========
        lon (xr.DataArray, np.array):
                the Longitude Vector
        lat (xr.DataArray, np.array):
                the Latitude Vector
        dset (xr.DataArray):
                The data to be plotted
        outfile (str, pathlib.Path):
            The filename of the animation
        plot_range (xr.DataArray):
                xarray DataArray with the timesteps to be considered
    """
    pargs = (dset.sel(time=i) for i in plot_range)
    res = []
    with TemporaryDirectory(prefix='map_plotter') as td:
        args = (lon, lat) + args
        kwargs['dirname'] = td
        plotter = partial(_pool_wrapper, func=plot_map, args=args, kwargs=kwargs)
        with mp.Pool(processes=10) as pool:
            pool.map(plotter, pargs)
        return _create_animation(td, outputfile)


def main(cfg):

    data = cfg['data'].load()
    try:
        arg = data['rotated_pole'].attrs
        lon = data['rlon'].data
        lat = data['rlat'].data
        mapping_method = 'from_rotated_grid'
        lonlatbox = None
    except KeyError:
        arg = cfg['projection'] or 'Mollweide(50)'
        lat = data['lat'].data
        lon = data['lon'].data
        mapping_method = 'from_global_grid'
    attrs = data.attrs
    if 'lev' in data.dims:
        if cfg['level'] is None or cfg['level'] == 0:
            data = data.isel(lev=0)
        else:
            lev = np.argmin(np.fabs(data.coords['lev'].values - cfg['level']))
            data = data.isel(lev=lev)
    if cfg['time_mean']:
        data = getattr(data.resample(time=time_interval), cfg['time_method'])()
        data.attrs = attrs
    vmin = cfg['vmin']
    vmax = cfg['vmax']
    cbar_label = cfg['cbar_label']
    title = cfg['plot_title'] or ' '
    fig_size = tuple(np.array(cfg['pic_size']) / 150) # pixel to inch
    outfile = Path(cfg['outfile'])
    data = data.load()
    P =  animate(lon, lat, data, outfile, data.time, arg,
                 cbar_label=cbar_label,
                 title=title,
                 mapping_method=mapping_method,
                 color=cfg['linecolor'],
                 vmax=vmax,
                 vmin=vmin,
                 figsize=fig_size,
                 cmap=cfg['cmap'])


def create_global_animation(dset, file_name, cbar_label,
                            overwrite=False,
                            vmin=None,
                            vmax=None,
                            cmap='RdYlBu_r',
                            proj='Mollweide(50)',
                            mul=1):
    """Create an animation of a given data array."""
    file_name = Path(file_name)
    file_name.parent.mkdir(exist_ok=True, parents=True)
    if file_name.is_file() and not overwrite:
        return str(file_name)
    dset = dset.load() * mul
    cfg = dict(outfile=Path(file_name),
               data=dset,
               cmap=cmap,
               vmin=vmin,
               vmax=vmax,
               cbar_label=cbar_label,
               plot_title=' ',
               pic_size=(1360, 900),
               time_method=None,
               time_mean=False,
               level=None,
               projection=proj,
               linecolor='black',)
    main(cfg)

