from copy import deepcopy
from itertools import product
import logging
from pathlib import Path
import os
import sys
import warnings

import colorlover as cl
from distributed import wait
import dask
import numpy as np
import pandas as pd
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import xarray as xr
from cartopy import crs as ccrs
import hvplot
import holoviews as hv
import hvplot.xarray
import geoviews as gv
import geoviews.feature as gf
import panel as pn

from ..utils import get_minmax, Averager, SCRATCH_DIR, Progress, run_cmd

warnings.filterwarnings('ignore', category=UserWarning)
warnings.filterwarnings('ignore', category=RuntimeWarning)
logging.disable(logging.WARNING)


__all__ = ('lineprop', 'update_layout', 'add_trace', 'scatter_layout',
           'plot_map_comparison', 'plot_maps')

def lineprop(num, dash, width, exp):
    colorset = cl.to_rgb(cl.scales['12']['qual']['Paired'])
    return dict(color=colorset[num], dash=dash, width=width)


def scatter_layout(fig,
                   all_traces,
                   surftypes,
                   vn_traces,
                   visible,
                   vn_lookup=None,
                   buttons=dict()):
    """Update a plotly layout"""
    x_buttons, y_buttons, surf_buttons = [], [], []
    yaxis = fig.layout.yaxis
    annotations = list(fig.layout.annotations)
    for nn, surf in enumerate(surftypes):
        vis = np.full_like(visible, False) 
        vis[nn::len(surftypes)] = True
        surf_buttons.append(dict(label=surf, method='update', 
                            args=[{'visible': vis.tolist()}]))
    for varname, data in vn_traces.items():
        x, y = [ts.x for ts in data], [ts.y for ts in data]
        txt = vn_lookup[varname][0]
        ann_y = deepcopy(yaxis)
        ann_x = list(deepcopy(annotations))
        ann_y['title'] = txt
        ann_x[-2]['text'] = txt
        x_buttons.append(dict(label=f'X:{varname}', method='update',
                              args=[{'x':x}, {'annotations': ann_x}]))
        y_buttons.append(dict(label=f'Y:{varname}', method='update', 
                              args=[{'y':y}, {'yaxis': ann_y.update({'title': {'text': txt}})}]))
    fig.update_layout(
        updatemenus=[
                buttons,
                dict(
                    buttons=x_buttons,
                    direction="down",
                    pad={"r": 10, "t": 10},
                    #showactive=True,
                    active=0,
                    x=0.97,
                    xanchor="left",
                    y=1.2,
                    yanchor="top"
                ),
            dict(
                    buttons=y_buttons,
                    direction="down",
                    pad={"r": 10, "t": 10},
                    #showactive=True,
                    active=0,
                    x=0.65,
                    xanchor="left",
                    y=1.2,
                    yanchor="top"
                ),
            dict(
                    buttons=surf_buttons,
                    direction="down",
                    pad={"r": 10, "t": 10},
                    #showactive=True,
                    active=0,
                    x=0.05,
                    xanchor="left",
                    y=1.2,
                    yanchor="top"
                ), 
        ]
    )
    return fig

def update_layout(fig,
                  varnames,
                  surftypes,
                  vn_lookup=None,
                  label_axis='yaxis',
                  times=None,
                  surf_type='buttons'):
    var_buttons, surf_buttons, sliders = [], [], []
    annotations = fig.layout.annotations
    laxis = fig.layout[label_axis]
    plot_array = np.array(list(product(varnames, surftypes.keys())))
    for nn, vn in enumerate(varnames):
        visible = len(varnames)*[False]
        visible[nn] = True
        txt = vn_lookup[vn][0]
        ann = deepcopy(laxis)
        var_buttons.append(dict(label=vn, method='update',
                            args=[{'visible': visible},
                                  {label_axis:ann.update({'title': {'text': txt}})}]))
    for surftype, data in surftypes.items():
        x, y = [ts.x for ts in data], [ts.y for ts in data]
        surf_buttons.append(dict(label=surftype, method='update', args=[{'y':y, 'x':x}]))
        sliders.append(dict(label=surftype, method='update', args=[{'y':y, 'x':x}]))
    text = vn_lookup[varnames[0]][0]
    ann = dict(x=0, y=1.35, xanchor='left', yanchor='top', showarrow=False,
               xref='paper', yref='paper', font=dict(size=18), text=text)
    if surf_type.lower() != 'buttons':
        sliders = [dict(
            active=0,
            currentvalue={"prefix": "Time: "},
            pad={"t": 50},
            steps=sliders)]
        surf_buttons = dict()
    else:
        sliders = None
        surf_buttons = dict(
                    buttons=surf_buttons,
                    direction="down",
                    pad={"r": 10, "t": 25},
                    active=0,
                    x=0.4,
                    xanchor="left",
                    y=1.35,
                    yanchor="top"
                )
    fig.update_layout({
        label_axis:laxis.update({'title': {'text': vn_lookup[varnames[0]][0]}}),
        'updatemenus':[
                dict(
                    buttons=var_buttons,
                    direction="down",
                    pad={"r": 10, "t": 25},
                    active=0,
                    x=0.97,
                    xanchor="left",
                    y=1.35,
                    yanchor="top"
                ),
            surf_buttons,
        ]
    }, sliders=sliders)
    return fig

def add_trace(data, exp, lineprop, visible=True, showlegend=True, ylim=None, xlim=None, mode='lines'):

    if len(exp) == 0:
        showlegend = False
    ts  = go.Scatter(x=data.index,
                     y=data.values,
                     name=exp,
                     showlegend=showlegend,
                     line=lineprop,
                     legendgroup=exp,
                     visible=visible,
                     mode=mode
                    )
    return ts


@dask.delayed
def save_mapfig(dset, var_name, save_file, label='', diff=None, diff_name='obs', **kwargs):
    
    """Save a holoview map as bookeh format"""
    renderer = gv.renderer('bokeh')
    gv.extension('bokeh')
    gv_dset =  gv.Dataset(xr.Dataset(data_vars={var_name:dset}, coords=dset.coords),
                                     kdims=['time', 'lat', 'lon'],
                                     vdims=var_name)
    gv_diff = None
    if diff is not None:
        gv_diff = gv.Dataset(xr.Dataset(data_vars={var_name:diff}, coords=diff.coords),
                                     kdims=['time', 'lat', 'lon'],
                                     vdims=var_name)
    defaults = dict(cmap='RdBu_r',
                    colorbar=True,
                    projection=None,
                    width=600,
                    height=300,
                    backend='bokeh',
                    clabel=var_name)
    for key, value in defaults.items():
        kwargs.setdefault(key, value)
    img = gv_dset.to(gv.Image, ['lon', 'lat'], var_name, label=f'{label} :').opts(**kwargs) * gf.coastline()
    if diff is not None:
        kwargs['cmap'] = 'RdBu_r'
        kwargs['clabel'] = 'diff'
        diff_img = gv_diff.to(gv.Image, ['lon', 'lat'], var_name, label=f'{label} - {diff_name} :').opts(**kwargs) * gf.coastline()
        img = hv.Layout([img, diff_img]).cols(1)
    plot = pn.panel(img, center=True, widget_location='bottom')
    plot.save(str(save_file.with_suffix('')), title=var_name, embed=True)
    del plot, img, gv_dset, gv_diff
    return str(save_file)

def plot_map_comparison(dset1,
                        dset2,
                        figure_path,
                        monthr,
                        config,
                        all_symmetric=False,
                        prefix='ceresvalue',
                        cmap=None,
                        overwrite=[],
                        subplots=None,
                        proj=None):
    minmax = {}
    for data_var in dset1.data_vars:
        symmetric = True
        if data_var in ('hfss', 'hfls', 'net_surf_energy', 'net_surf') and not all_symmetric:
            symmetric = False
        minmax[data_var] = get_minmax(dset1[data_var], dset2[data_var],
                                      symmetric=symmetric, ts=0)
    minmax = dask.compute(minmax)[0]
    filenames = []
    for data_var in dset2.data_vars:
        for exp_name in dset1.coords['exp'].values:
            target_file = Path(figure_path) / f'{prefix}_{exp_name.replace(" ","")}_{data_var}.html'
            _cmap = cmap or config['variables']['desc'][data_var][1]
            label = config['variables']['desc'][data_var][0]
            t1 = max(monthr[exp_name][0], pd.Timestamp(dset2.time.values[0]))
            t2 = min(monthr[exp_name][1], pd.Timestamp(dset2.time.values[-1]))
            tmp_df1 = dset1.sel({'exp': exp_name, 'time': slice(t1, t2)})[data_var]
            tmp_df2 = dset2[data_var].sel({'time': slice(t1, t2)}).isel({'exp':0})
            cmin, cmax, dmax, dmin = minmax[data_var][:]
            tmp_diff = (tmp_df2 - tmp_df1.data).clip(-np.fabs(dmin), np.fabs(dmax))
            if target_file.is_file() and exp_name not in overwrite:
                continue
            filenames.append(save_mapfig(tmp_df2.clip(cmin, cmax),
                                         data_var,
                                         target_file,
                                         projection=proj, cmap=_cmap,
                                         label=str(dset2.coords['exp'].values[0]),
                                         diff=tmp_diff,
                                         diff_name=str(exp_name)))
    return filenames

def plot_maps(dset,
              figure_path,
              monthr,
              config,
              all_symmetric=False,
              prefix='absvalue',
              cmap=None,
              overwrite=[],
              subplots=None,
              proj=None):
    minmax = {}
    for data_var in dset.data_vars:
        symmetric = True
        if data_var in ('hfss', 'hfls', 'net_surf_energy', 'net_surf') and not all_symmetric:
            symmetric = False
        minmax[data_var] = get_minmax(dset[data_var], symmetric=symmetric)
    minmax = dask.compute(minmax)[0]
    filenames = []
    for data_var in dset.data_vars:
        for exp_name in dset.coords['exp'].values:
            target_file = Path(figure_path) / f'{prefix}_{exp_name.replace(" ","")}_{data_var}.html'
            _cmap = cmap or config['variables']['desc'][data_var][1]
            label = config['variables']['desc'][data_var][0]
            tmp_df = dset.sel({'exp': exp_name, 'time': slice(monthr[exp_name][0], monthr[exp_name][-1])})
            cmin, cmax = minmax[data_var][:2]
            if target_file.is_file() and exp_name not in overwrite:
                continue
            #filenames.append(tmp_df[data_var].clip(cmin, cmax))
            filenames.append(save_mapfig(tmp_df[data_var].clip(cmin, cmax), data_var, target_file,
                                         projection=proj, cmap=_cmap, label=str(exp_name)))
    return filenames
