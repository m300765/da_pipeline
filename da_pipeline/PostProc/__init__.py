
from tempfile import TemporaryDirectory
from pathlib import Path

import dask
from dask.distributed import wait
import numpy as np
import pandas as pd
import xarray as xr

from ..utils import Averager, SCRATCH_DIR, run_cmd, Progress
from ..ReduceFactory import Regridder
from .PlotMap import (lineprop, update_layout, add_trace, scatter_layout,
        plot_map_comparison, plot_maps)
from .Animator import create_global_animation

__all__ = ('time_avg', 'fld_avg', 'remap_to_target', 'zonal_mean' ,'create_global_animation') + \
('lineprop', 'update_layout', 'add_trace', 'scatter_layout', 'plot_map_comparison', 'plot_maps')

def zonal_mean(dset, land_frac, land_type):
    """Calculate a zonal mean"""
    try:
        dset = dset * land_frac[land_type].data
    except:
        pass
    return dset.mean(dim='lon')

def _calc_fld_avg(dset, lsm, conf, expname, surftypes, skipna):

    futures = {}
    for area in conf.keys():
        try:
            box = conf[area]['lonlatbox']
        except TypeError:
            continue
        futures[area] = []
        try:
            sel = {'lat': slice(*box[2:]), 'lon': slice(*np.array(box[:2])+180)}
        except TypeError:
            sel = None
        data = dset.sel(sel)
        for surf in surftypes:
            try:
                mask = lsm[surf].sel(sel)
            except KeyError:
                mask = None
            futures[area].append(Averager.fldmean(data, land_frac=mask, skipna=skipna).persist())
        Progress(futures[area], label=f'Reading {area} / {expname}', notebook=False)
    out = {}
    for area, future in futures.items():
        fld_mean = xr.concat(future, dim='surf').assign_coords({'surf':list(surftypes.values())})
        out[area] = fld_mean.load()
    fld_mean = xr.concat(list(out.values()), dim='area').assign_coords({'area': list(out.keys())})
    return xr.concat([fld_mean], dim='exp').assign_coords({'exp': [expname]}).load()

def fld_avg(dset, lsm, conf, expname, save_path, suffix, overwrite, skipna=False):
    surftypes = {None: 'Land+Ocean', 'sea': 'Ocean', 'land': 'Land'}
    times = pd.DatetimeIndex([dset.time[0].values, dset.time[-1].values])
    out_file = Path(save_path) / f'{suffix}_{expname}.nc'
    out_file.parent.mkdir(exist_ok=True, parents=True)
    try:
        dset_read = xr.open_dataset(out_file)
        old_times = pd.DatetimeIndex([dset_read.time[0].values,
                                      dset_read.time[-1].values])
    except (KeyError, FileNotFoundError, OSError):
        dset_read = None
    if dset_read is not None:
        if all(times == old_times) and expname not in overwrite:
            return dset_read
    time_series = _calc_fld_avg(dset, lsm, conf, expname, surftypes, skipna)
    try:
        out_file.unlink()
    except (FileNotFoundError, OSError):
        pass
    time_series.to_netcdf(out_file, mode='w', compute=False).persist()
    return time_series






def time_avg(dataset, min_size=15, resample={'time':'1m'}):
    """Create monthly average"""
    mon_avg = []
    for nn, (time, dset) in enumerate(dataset.resample(resample)):
        if nn == 0 or dset.time.shape[0] >= min_size:
            mon_avg.append(dset.mean(dim='time').assign_coords({'time': time}))
    return xr.concat(mon_avg, dim='time')

#def fldmean_lonlat(data, land_frac, area, lonlatbox, surftype=None):
#    lonlatbox = np.array(lonlatbox)
#    if ((data.lon.max() > 180) and (data.lon.min()) > 0) and np.all(lonlatbox) and area !='tropics':
#        lonlatbox[:2] = lonlatbox[:2] % 360
#    try:
#        extent = {k: slice(*v) for k, v in get_extent(list(lonlatbox)).items()}
#    except TypeError:
#        extent = None
#    if area != 'global':
#        data = data.sel(extent)
#        land_frac = land_frac.sel(extent)
#    if surftype is not None:
#        mask = land_frac[surftype].data
#    else:
#        mask = 1
#    return Averager._weighted(data, ('lat', 'lon'), mask)

def remap_to_target(source_data, target_data, expname='', griddes=None, cdo='cdo'):
    griddes = griddes or Regridder.get_grid_from_dataset(target_data)
    futures = {}
    try:
        source_data = source_data.sel({'exp':expname}).drop('exp')
    except (KeyError, ValueError):
        pass
    return _remap_to_target(source_data, griddes, expname, cdo=cdo)

#@dask.delayed
def _remap_to_target(dataset_in, griddes, expname, cdo='cdo'):
    with TemporaryDirectory(dir=SCRATCH_DIR, prefix='target_remap') as td:
        tmp_dir = Path(td)
        griddesfile = tmp_dir / 'griddes.txt'
        with griddesfile.open('w') as f:
            f.write(griddes)
        old_f = tmp_dir / 'input_file.nc'
        new_f = tmp_dir / 'output_file.nc'
        write_future = dataset_in.to_netcdf(old_f, compute=False).persist()
        Progress(write_future, label=f'Remapping {expname}', notebook=False)
        cmd = cdo, '-O', f'remapcon,{griddesfile}', f'{old_f}', f'{new_f}'
        remap_future = run_cmd(cmd).compute()
        df = xr.open_mfdataset([new_f]).persist()
        wait(df)
        return df

