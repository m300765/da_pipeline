"""Collection of methods for handling swift object storage."""

from datetime import datetime, timedelta
from functools import partial
import getpass
import logging
import os
import os.path as osp
from pathlib import Path
import requests
import stat
import sys
import time

import swiftclient
from swiftclient.multithreading import OutputManager
from swiftclient.service import SwiftError, SwiftService, SwiftUploadObject


logging.basicConfig(level=logging.ERROR)
logging.getLogger("requests").setLevel(logging.CRITICAL)
logging.getLogger("swiftclient").setLevel(logging.CRITICAL)
logger = logging.getLogger(__name__)

SWIFT_BASE = 'https://swift.dkrz.de/'
SWIFT_PUBLIC = f'{SWIFT_BASE}v1'
SWIFT_VERS='v1.0'

def _getUploadObject(inp_dir, path, options=None):
    """Make upload object from a path."""
    if options:
        sw_upload = partial(SwiftUploadObject, options=options)
    else:
        sw_upload = SwiftUploadObject

    swift_file = Path(path).relative_to(Path(inp_dir).parent)
    return sw_upload(str(path), str(swift_file))

def _get_filenames(inp_dir):
    """Walk through a given directory and return all filenames."""

    inp_dir = str(inp_dir)
    objs = []
    dir_markers = []
    opts = {'dir_marker': True}
    for (_dir, _ds, _fs) in os.walk(inp_dir):
        if not (_ds + _fs):
            dir_markers.append(_getUploadObject(inp_dir, _dir, options=opts))
        else:
            objs.extend([_getUploadObject(inp_dir, osp.join(_dir, _f)) for _f in _fs])
    return objs + dir_markers


def _print_status(res, container, print_success=False, **kwargs):
    """Print status of swift a operation."""

    if res['success']:
        if print_success:
            if 'object' in res:
                print(res['object'])
            elif 'for_object' in res:
                print(
                    '%s segment %s' % (res['for_object'],
                                       res['segment_index'])
                    )
    else:
        error = res['error']
        if res['action'] == "create_container":
            logger.warning(
                'Warning: failed to create container '
                "'%s'%s", container, error
            )
        elif res['action'] == "upload_object":
            logger.error(
                "Failed to upload object %s to container %s: %s" %
                (container, res['object'], error)
            )
        else:
            logger.error("%s" % error)


def _check_connection(account, user, password=None):

    headers = {'X-Auth-User': f"{account}:{user}"}
    if password:
        headers['X-Auth-Key'] = password
    resp = requests.get(SWIFT_BASE + f'auth/{SWIFT_VERS}', headers=headers)
    try:
        resp.raise_for_status()
    except requests.RequestException:
        raise requests.RequestException(f'Connection to {SWIFT_BASE} failed')
    return resp

def create_token(account, user, password=None,**kwargs):
    """Create a new swift token for a given account."""

    user = user or getpass.getuser()
    if not password:
        password = getpass.getpass((f'Creating new swift-token'
                                    f'\nGive password for {user} on {account}: '))
    resp = _check_connection(account, user, password)
    tz = time.tzname[-1]
    expires = token = storage_url = None
    token = resp.headers["x-auth-token"]
    storage_url = resp.headers["x-storage-url"]
    expires_in = int(resp.headers["x-auth-token-expires"])
    expires_at = datetime.fromtimestamp(time.time() + expires_in)
    expires = expires_at.strftime('%a %d. %b %H:%M:%S {tz} %Y'.format(tz=tz))
    write_token(token, storage_url, expires, **kwargs)


def write_token(token, storage_url, expires, token_file='~/.swiftenv'):
    """Write credentials to a token file for permanent usage.

    Parameters:
    ==========
        token: str
            New swift login token
        storage_url: str
            url associated with a swift container and account
        expires: str
            expiry date of the token
        token_file : str, pathlib.Path, default: ~/.swiftenv
            filename where credentials are stored.
    """

    env = {'OS_AUTH_TOKEN': token, 'OS_STORAGE_URL': storage_url,
           'OS_AUTH_URL': '" "', 'OS_USERNAME': '" "',
           'OS_PASSWORD': '" "'}
    with open(osp.expanduser(token_file), 'w') as f:
        f.write(f'#token expires on: {expires}\n')
        for key, value in env.items():
            f.write(f'setenv {key} {value}\n')
    os.chmod(osp.expanduser(token_file), stat.S_IREAD | stat.S_IWRITE)


def _connection_isvalid(account, username):

    try:
        with Path('~/.da_swift.conf').expanduser().open('r') as f:
            user = f.readline().strip().strip('\n').lower() == username.lower()
            acc = f.readline().strip().strip('\n').lower() == account.lower()
    except FileNotFoundError:
        user, acc = False, False
    with Path('~/.da_swift.conf').expanduser().open('w') as f:
        f.write(username+'\n')
        f.write(account+'\n')
    return all((user, acc))


def check(account, username=None, password=None, **kwargs):
    """Check if a swift token for a given account is still valid.

    This method checks if a token for a swift container can be used
    to log on to swift and if the token is valid for at least one day.
    Otherwise a new token will be created.

    Parameters:
    ===========
        account : str
            Account name of the associated swift container
        username : str (default None)
            User name for the swift token, this system will get the
            username the runs this program if None is given (default)
    Returns:
    ========
        dict: Dictionary containing the swift credentials
    """

    try:
        env, expires = _get_env(**kwargs)
    except FileNotFoundError:
        env, expires = {}, datetime.now()
    valid = _connection_isvalid(account, username or getpass.getuser())
    if (expires - datetime.now()).total_seconds() / 60**2 > 24\
       and env.get('OS_AUTH_TOKEN', '') and env.get('OS_STORAGE_URL') and valid:
        return env
    create_token(account, username or None, password=password or None)
    return _get_env(**kwargs)


def _get_env(token_file='~/.swiftenv', **kwargs):
    """Read current swift credentials."""

    env = {}
    with open(os.path.expanduser(token_file)) as f:
        for nn, line in enumerate(f.readlines()):
            if nn == 0 and line.startswith('#'):
                date_str = line.strip('\n').partition(':')[-1].strip()
                expires = datetime.strptime(date_str, '%a %d. %b %H:%M:%S %Z %Y')
            if line.startswith('setenv'):
                content = line.strip('\n').partition(' ')[-1].split(' ')
                value = ' '.join(content[1:]).strip('"').strip("'")
                env[content[0]] = value
    return env, expires


def _get_opts(**kwargs):
    """Get connection options for swift."""

    env, _ = _get_env(**kwargs)
    return {'help': False, 'os_help': False, 'snet': False, 'verbose': 1,
             'debug': False, 'info': False, 'auth': None,
             'auth_version': '2.0', 'user': ' ', 'key': ' ', 'retries': 5,
             'insecure': False, 'ssl_compression': True,
             'force_auth_retry': False, 'prompt': False, 'os_username': ' ',
             'os_user_id': None, 'os_user_domain_id': None,
             'os_user_domain_name': None, 'os_password': ' ',
             'os_tenant_id': None, 'os_tenant_name': None,
             'os_project_id': None, 'os_project_name': None,
             'os_project_domain_id': None, 'os_project_domain_name': None,
             'os_auth_url': None, 'os_auth_type': None,
             'os_application_credential_id': None,
             'os_application_credential_secret': None,
             'os_auth_token': env['OS_AUTH_TOKEN'],
             'os_storage_url': env['OS_STORAGE_URL'],
             'os_region_name': None, 'os_service_type': None,
             'os_endpoint_type': None, 'os_cacert': None, 'os_cert': None,
             'os_key': None, 'changed': False, 'skip_identical': False,
             'segment_size': None, 'segment_container': None,
             'leave_segments': False, 'object_threads': 20,
             'segment_threads': 20, 'meta': [], 'header': [],
             'use_slo': False, 'object_name': None, 'checksum': True,
             'os_options': {'user_id': None, 'user_domain_id': None,
                            'user_domain_name': None, 'tenant_id': None,
                            'tenant_name': None, 'project_id': None,
                            'project_name': None, 'project_domain_id': None,
                            'project_domain_name': None, 'service_type': None,
                            'endpoint_type': None,
                            'auth_token': env['OS_AUTH_TOKEN'],
                            'object_storage_url': env['OS_STORAGE_URL'],
                            'region_name': None, 'auth_type': None,
                            'application_credential_id': None,
                            'application_credential_secret': None
                            },
             'object_uu_threads': 20}


def upload(account, inp_dir, container, **kwargs):
    """Upload a folder to a given swift-container.

    Parameters:
    ===========
        account: str
            Account name of the associated swift container
        inp_dir: str, pathlib.Path
            Source input folder that is going to be uploaded
        container: str
            Name of the swift container
    """
    inp_dir = Path(inp_dir)
    parent = inp_dir.name
    _opts = _get_opts(**kwargs)
    with SwiftService(options=_opts) as swift, OutputManager() as out_manager:
        objs = _get_filenames(str(inp_dir))
        try:
            for r in swift.upload(container, objs):
                _print_status(r, container, **kwargs)
        except SwiftError as e:
            logger.error(e.value)
        key = swift.stat(container)['items'][0][1]
    container_url = f'{SWIFT_PUBLIC}/{key}/{container}/{parent}'
    print(f'Content successfully uploaded to {container_url}/index.html')
    return container_url
