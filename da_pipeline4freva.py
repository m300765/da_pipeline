
import json
import os
import sys
from tempfile import NamedTemporaryFile
import toml

class mock_user:

    def __init__(self, *args, **kwargs):
        return

    def getUserOutputDir(self, tool=None, create=False):
        return '.'

class mock_class:

    _special_variables = []
    def __init__(self, *args, **kwargs):
        return

    @classmethod
    def ParameterDictionary(cls, *args):
        return

    @classmethod
    def String(cls, **kwargs):
        return

    @classmethod
    def Float(cls, **kwargs):
        return

    @classmethod
    def Integer(cls, **kwargs):
        return

    @classmethod
    def Directory(cls, **kwargs):
        return

    @classmethod
    def File(cls, **kwargs):
        return

    @classmethod
    def Bool(cls, **kwargs):
        return

try:
    from pathlib2 import Path
except ImportError:
    from pathlib import Path
try:
    from evaluation_system.api import plugin, parameters
    from evaluation_system.misc import config
    from evaluation_system.model.user import User
except ImportError:
    from collections import namedtuple
    plugin = namedtuple('mock_class', 'PluginAbstract')
    plugin.PluginAbstract = mock_class
    parameters = mock_class
    User = mock_user
    config = {}


MASTER_CFG = Path(__file__).absolute().parent / '.config_tmpl.json'
try:
    with open(str(MASTER_CFG)) as f:
        OLD_CFG = json.load(f)
except:
    OLD_CFG = {}

CFG = dict(\
    steps=('run_2d_reduction, run_3d_reduction, run_notebook',
           'Define the steps the pipeline will apply: '
           'run_2d_reduction: Run processing of 2D data '
           'run_3d_reduction: Run processing of 3D data '
           'run_notebook: Run data visualisation'
           , str),
    exp_desc=("Coupled 3D smagorinsky physics <b>(inhom fact. 0.66)</b> at 5 km, 90 vertcial levels",
              "Give an informative describtion of the experiment", str),
    save_config=(True,"Save the configuration for later use", bool),
    slurm_account=('ch1187', 'Set the slurm account for computation', str),
    slurm_run_3d_reduction_queue=('compute', 'Set the slurm partion for the 3D data processing', str),
    slurm_run_3d_reduction_cores=(48, 'Set the number of cores per node for the 3D data processing', int),
    slurm_run_3d_reduction_memory=(60, 'Set memory (in GB) per node for the 3D data processing', int),
    slurm_run_3d_reduction_workers=(25, 'Set the number of nodes for the 3D data processing', int),
    slurm_run_3d_reduction_walltime=('5:30:00', 'Set the walltime for the 3D data processing', str),
    slurm_run_3d_reduction_extra=('--begin=now', 'Set additional arguments passed to slurm (for 3D data processing)', str),
    slurm_run_2d_reduction_queue=('compute', 'Set the slurm partion for the 2D data processing', str),
    slurm_run_2d_reduction_cores=(48, 'Set the number of cores per node for the 2D data processing', int),
    slurm_run_2d_reduction_memory=(60, 'Set the memory (in GB) per node for the 2D data processing', int),
    slurm_run_2d_reduction_workers=(25, 'Set number of workers for the 2D data processing', int),
    slurm_run_2d_reduction_walltime=('5:30:00', 'Set the walltime for the 2D data processing', str),
    slurm_run_2d_reduction_extra=('--begin=now', 'Set additional arguments passed to slurm (for 3D data processing)', str),
    slurm_run_notebook_queue=('compute', 'Set the slurm partion for the data visualisation step', str),
    slurm_run_notebook_cores=(48, 'Set the number of cores per node for the data visualisation step', int),
    slurm_run_notebook_memory=(100, 'Set the memory (in GB) per node for the data visualisation step', int),
    slurm_run_notebook_workers=(25, 'Set the number of workers for the data visualisation step', int),
    slurm_run_notebook_walltime=('5:30:00', 'Set the walltime for the data visualisation step', str),
    slurm_run_notebook_extra=('--begin=now', 'Set additional arguments passed to slurm (for data visualisation)', str),
    swift_account=('mh0287', 'Set the swift group account where the visualisation data will be uploaded to', str),
    swift_container=('Dyamond_plus', 'Set the name of the swift container where the result page is hosted', str),
    exp_name=('dpp0014', 'Set the experiment short name', str),
    path=('/work/mh0287/k203123/GIT/icon-aes-dyw2/experiments/dpp0029',
          'Set the data path to the experiment output', str),
    out_dir=('/mnt/lustre01/work/mh0731/m300765/DyamondWinter',
          'Set the output path to where the post-processed data and plots are store'
          ' Note: it is advisable to set this path only once and reuse it again', str),
    start=('20200120T000000', 'Set the first time step to be processed (yyyymmddTHHMM format)', str),
    end=('20200301T000000', 'Set the last time step to be processed (yyyymmddTHHMM format)', str),
    run_type=('dpp', 'Set the runtime (dpp or nwp)', str),
    model_type=('atm', 'Set the model data type (atm: atmosphere, oce: ocean, lnd: land', str),
    avg_period=('1D', 'Set the averaging period (1D: daily averages etc), leave blank or set to None for no averaging', str),
    target_res=('0.5, 0.5', 'Target resolution (lon x lat) of the post porcessed data (3D and 2D reduction)', list),
    glob_pattern_2d=('atm_2d_ml', 'Set the glob pattern that is applied to search for 2D experiment input data', list),
    glob_pattern_3d=('atm_3d_1_ml, atm_3d_2_ml, atm_3d_3_ml, atm_3d_4_ml, atm_3d_5_ml', 'Set the glob pattern that is applied to search for 3D experiment input data', list),
    grid_file=('/pool/data/ICON/grids/public/mpim/0015/icon_grid_0015_R02B09_G.nc', 'Set the path to the grid file', Path),
    vgrid_file=('/mnt/lustre01/work/mh0731/m300765/da_pipeline/r02b09/r02b09_atm_vgrid_ml.nc', 'Set the path to the vertical grid file', Path),
    min_height=(25, 'Set the height of the lowest level above ground (in m) for the target iso z level', int),
    max_height=(35000, 'Set the height of the highest level above ground (in m) for the target iso z level', int),
    vis_map_projection=('Mollweide(50)', 'Set the Cartopy map porjection call', str),
    vis_map_transform=('PlateCarree()', 'Set the  Cartopy data transformation call', str),
    vis_long_title=("Dyamond Winter (boreal winter) simulation comparison",
                    "Set a short Project describtion", str),
    vis_short_title=("Dyamond Winter", 'Set a short (and descriptive) project name', str),
    vis_datasets=("dpp0014, dpp0015, dpp0016, dpp0018, dpp0020, dpp0029, nwp, nwp0005", "Select the dataset that are included in the visualisation", list),
    )

_slurm_options = []
for step in CFG['steps'][0].strip().split(','):
    step = step.strip()
    cfg_order = ('queue', 'workers', 'cores', 'memory', 'walltime', 'extra')
    if step:
        _slurm_options += ['slurm_{}_{}'.format(step, opt) for opt in cfg_order]

_field_order = ('vis_short_title', 'vis_long_title', 'exp_desc', 'steps',
                'slurm_account', 'start', 'end',
                'swift_account', 'swift_container', 'run_type',
                'glob_pattern_2d', 'glob_pattern_3d', 'out_dir',
                'grid_file', 'vgrid_file', 'target_res', 'avg_period',
                'min_height', 'max_height', 'vis_map_projection',
                'vis_map_transform')+tuple(_slurm_options)+('vis_datasets',
                        'save_config')

for key in OLD_CFG.keys():
    try:
        CFG[key] = (OLD_CFG[key], CFG[key][1], CFG[key][-1])
    except KeyError:
        pass


def get_parameter_method(name):
    value, desc, instance = CFG[name]
    kwargs = dict(name=name, default=value, help=desc, mandatory=False)
    if instance == str:
        return parameters.String(**kwargs)
    elif instance == float:
        return paramerters.Float(**kwargs)
    elif instance == int:
        return parameters.Integer(**kwargs)
    elif instance == dir:
        return parameters.Directory(**kwargs)
    elif instance == Path:
        return parameters.File(**kwargs)
    elif instance in (list, tuple, set):
        return parameters.String(**kwargs)
    elif instance == bool:
        return parameters.Bool(**kwargs)
    return parameters.String(**kwargs)

class DaPipeline(plugin.PluginAbstract):
    __category__ = 'postproc'
    __tags__ = ['plotting',]
    __short_description__ = "Data Analysis Pipelines for processing and visualizing ICON ouptput data. "
    __long_description__   = """
   This plugin sets up an automatic pipeline for distributed data processing. The pipeline consists of different
   steps for data processing (reduction), data visualisation and uploading the content to a swift cloud object storage.
    """

    __version__ = (2021, 4, 12)

    __parameters__ = parameters.ParameterDictionary(
        parameters.Directory(name='freva_output_directory',
                             default='$USER_OUTPUT_DIR/$SYSTEM_DATETIME',
                             mandatory=True,
                             help='The output directory where some plots will be saved for inspection.'),
        parameters.String(name='exp_name', mandatory=True, help=CFG['exp_name'][1]),
        parameters.File(name='path', mandatory=True, help=CFG['exp_name'][1]),
        *[get_parameter_method(name) for name in _field_order]
        )


    def runTool(self, config_dict={}):

        out_dir = Path(config_dict.pop('freva_output_directory')).expanduser().absolute()
        config = config_dict.copy()
        out_dir.mkdir(exist_ok=True, parents=True)
        config['project_name'] = config['vis_short_title'].strip().replace(' ','')
        master_config_path = Path(__file__).expanduser().absolute().parent
        with open(str(master_config_path / 'config.toml')) as f:
            master_config = toml.load(f)
        exp_name = config['exp_name']
        steps = config['steps']
        try:
            steps = steps.strip().replace(':',',').replace(';',',').split(',')
        except AttributeError:
            pass
        steps = [s.strip() for s in steps if s.strip()]
        master_config['general']['project_name'] = config['project_name']
        master_config['general']['steps'] = steps
        master_config['slurm']['account'] = config['slurm_account']
        for step in steps:
            for key in ('queue', 'cores', 'memory', 'workers', 'walltime'):
                key_lookup = 'slurm_{}_{}'.format(step, key)
                master_config['slurm'][step][key] = config[key_lookup]
            extra_ol = [k.strip() for k in config['slurm_{}_extra'.format(step, key)].split(',') if k.strip()]
            master_config['slurm'][step]['job_extra'] = extra_ol
        for key in ('account', 'container'):
            master_config['swift'][key] = config['swift_{}'.format(key)]
        for key in master_config['reduction'].keys():
            try:
                master_config['reduction'][key] = config[key]
            except KeyError:
                pass
        for key in ('target_res', 'glob_pattern_2d', 'glob_pattern_3d'):
            master_config['reduction'][key] = [k.strip() for k in config[key].split(',') if k.strip()]
        master_config['reduction']['target_res'] = [float(k) for k in master_config['reduction']['target_res']]
        for key in ('map_projection', 'map_transform'):
            master_config['visualisation'][key] = config['vis_{}'.format(key)]
        for key in ('long_title', 'short_title'):
            master_config['visualisation']['titles'][key] = config['vis_{}'.format(key)]
        datasets = [exp_name]
        for k in config['vis_datasets'].split(','):
            new = k.strip()
            if new and new not in datasets:
                datasets.append(new)
        master_config['visualisation']['overview']['datasets'] = sorted(datasets)
        master_config['visualisation']['overview']['resolution'][exp_name] = ''
        master_config['visualisation']['overview'][exp_name] = config['exp_desc']
        password = os.environ.get('LC_TELEPHONE', None)
        pipeline_path = str(master_config_path / 'env' / 'bin' / 'da-pipeline')
        try:
            with open(str(MASTER_CFG), 'w') as f:
                save_cfg = {}
                for k in config.keys():
                    if not k.startswith('slurm') and k != 'save_config':
                        save_cfg[k] = config[k]
                if config['save_config'] is True:
                    json.dump(save_cfg, f)
                else:
                    json.dump({}, f)
        except:
            pass
        with NamedTemporaryFile(prefix='{}_config'.format(config['exp_name']),
                                suffix='.toml') as tf:
            with open(tf.name, 'w') as f:
                toml.dump(master_config, f)
            cmd = '{} {} --foreground --freva_outdir {}'.format(pipeline_path,
                                                         tf.name,
                                                         str(out_dir))
            if password:
                cmd += " --password '{}'".format(password)
            res = os.system(cmd)
            if res != 0:
                raise RuntimeError('Job failed')
            return self.prepareOutput(str(out_dir))
