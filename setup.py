#!/usr/bin/env python
import os.path as osp
from pathlib import Path
import re
from setuptools import setup, find_packages
import sys


def get_script_path():
    return osp.dirname(osp.realpath(sys.argv[0]))


def read(*parts):
    return open(osp.join(get_script_path(), *parts)).read()

def find_files(path, glob_pattern='*'):
    return [str(f) for f in Path(path).rglob(glob_pattern)]

def find_version(*parts):
    vers_file = read(*parts)
    match = re.search(r'^__version__ = "(\d+.\d+)"', vers_file, re.M)
    if match is not None:
        return match.group(1)
    raise RuntimeError("Unable to find version string.")

setup(name="da_pipeline",
      version=find_version("da_pipeline", "__init__.py"),
      author="Max Planck Institute for Meteorology",
      maintainer="Martin Bergemann",
      description="Pipeline for processing and displaying climate/nwp data ",
      long_description=read("README.md"),
      long_description_content_type='text/markdown',
      license="BSD-3-Clause",
      packages=find_packages(),
      install_requires=[
          'cartopy',
          'cdo',
          'click >= 7.1.2',
          'colorlover',
          'cloudpickle',
          'dask',
          'dask-jobqueue==0.7.0',
          'distributed',
          'f90nml',
          'geoviews',
          'git-python',
          'ipywidgets',
          'ipython',
          'h5netcdf',
          'holoviews',
          'humanize',
          'hurry.filesize',
          'hvplot',
          'matplotlib',
          'nbsphinx',
          'metpy',
          'netcdf4',
          'numpy',
          'pandas',
          'papermill',
          'plotly',
          'proj',
          'progress',
          'psutil',
          'python-git',
          'stratify',
          'python-swiftclient',
          'pytest',
          'toml',
          'tqdm',
          'seaborn',
          'stratify',
          'xarray',
          ],
      extras_require={
        'docs': [
              'sphinx',
              'nbsphinx',
              'recommonmark',
              'ipython',  # For nbsphinx syntax highlighting
              'sphinxcontrib_github_alt',
              ],
        'test': [
              'pytest',
              'pytest-cov',
              'nbval',
              'h5netcdf',
              'testpath',
          ]
        },
      entry_points={'console_scripts': ['da_pipeline = da_pipeline._run:main',
                                         'da-pipeline = da_pipeline._run:main']},
      data_files=[('scripts', find_files(Path('da_pipeline') / 'scripts', '*.ipynb'))],
      package_data={'': ['scripts/*.ipynb'],},
      python_requires='>=3.6',
      classifiers=[
          'Development Status :: 3 - Alpha',
          'Environment :: Console',
          'Intended Audience :: Developers',
          'Intended Audience :: Science/Research',
          'License :: OSI Approved :: BSD License',
          'Operating System :: POSIX :: Linux',
          'Programming Language :: Python :: 3',
          'Programming Language :: Python :: 3.6',
          'Programming Language :: Python :: 3.7',
          'Programming Language :: Python :: 3.8',
          'Topic :: Scientific/Engineering :: Data Analysis',
          'Topic :: Scientific/Engineering :: Earth Sciences',
      ]
      )
